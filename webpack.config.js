/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

const path = require("path");
const LicenseWebpackPlugin = require("license-webpack-plugin").LicenseWebpackPlugin;

const additionalModules = [
  "material-design-icons",
  "source-sans-pro",
  "@angular/platform-browser-dynamic"
];

function renderLicenses(modules) {
  modules = modules
    .map((module) => {
      return {
        ...module.packageJson,
        licenseText: module.licenseText
      };
    });

  return modules
    .sort((a, b) => a.name.localeCompare(b.name))
    .reduce((_, module) => {
      const repositoryUrl = module.repository == null ? null : module.repository.url;
      return _ + "\n" +
        `${module.name} (${module.version})\n` +
        (module.license != null ? ` * License: ${module.license}\n` : "") +
        (module.homepage != null ? ` * Homepage: ${module.homepage}\n` : "") +
        (module.homepage == null && repositoryUrl != null ? ` * Repository: ${repositoryUrl}\n` : "")
    }, "");
}

module.exports = {
  plugins: [
    new LicenseWebpackPlugin({
      renderLicenses: (modules) => {
        return renderLicenses(modules);
      },
      perChunkOutput: false,
      additionalModules: additionalModules
        .map((name) => ({name, directory: path.join(__dirname, "node_modules", name)}))
    })
  ]
};
