*******************************************************************************
  Copyright (c) 2019 Contributors to the Eclipse Foundation

  See the NOTICE file(s) distributed with this work for additional
  information regarding copyright ownership.

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License v. 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
*******************************************************************************

# Nginx based docker image that contains the statement module frontend

This docker configuration creates a nginx based reverse-proxy.
It provides the statement module frontend at port 80.

Please go to the root folder to build this docker image.

## Configuration

The nginx configuration can be found in the default.conf file.

