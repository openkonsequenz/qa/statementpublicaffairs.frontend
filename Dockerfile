# ******************************************************************************
# Copyright (c) 2020 Contributors to the Eclipse Foundation
#
# See the NOTICE file(s) distributed with this work for additional
# information regarding copyright ownership.
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License v. 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
# ******************************************************************************

FROM nginx:1.18-alpine

COPY dist/statement-public-affairs /html-root

COPY docker/buildDocker/default.conf /etc/nginx/conf.d/default.conf

