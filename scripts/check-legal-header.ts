/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

"use strict";

const klaw = require("klaw");
const fs = require("fs");
const path = require("path");

const SEARCH_PATH = path.join(__dirname, "..");
const SEARCH_DIRECTORIES = [".storybook", "scripts", "src"];

const HEADER_TS = fs.readFileSync(path.join(__dirname, "legal-header-ts.txt")).toString();
const HEADER_HTML = fs.readFileSync(path.join(__dirname, "legal-header-html.txt")).toString();

async function getAllFiles(directory: string, depthLimit?: number): Promise<string[]> {
    return new Promise<string[]>((resolve, reject) => {
        const result: string[] = [];
        klaw(directory, {depthLimit})
            .on("data", (item) => item.stats.isDirectory() ? null : result.push(item.path))
            .on("error", (err) => reject(err))
            .on("end", () => resolve(result));
    });
}

async function getAllSearchFiles(searchPath: string, searchDirectories: string[]) {
    let result: string[] = [];
    for (const dir of searchDirectories) {
        result = [
            ...result,
            ...await getAllFiles(path.join(searchPath, dir))
        ];
    }
    return result;
}

function isFileHeaderCorrect(filePath: string, header: string) {
    const file = fs.readFileSync(filePath).toString();
    return file.trim().startsWith(header);
}

async function main() {

    const allFiles = await getAllSearchFiles(SEARCH_PATH, SEARCH_DIRECTORIES);

    const htmlFiles = allFiles
        .filter((file) => file.endsWith(".html"))
        .filter((file) => !isFileHeaderCorrect(file, HEADER_HTML.trim()));

    const tsFiles = allFiles
        .filter((file) => file.endsWith(".ts"))
        .filter((file) => !isFileHeaderCorrect(file, HEADER_TS.trim()));

    const scssFiles = allFiles
        .filter((file) => file.endsWith(".scss"))
        .filter((file) => !isFileHeaderCorrect(file, HEADER_TS.trim()));

    const cssFiles = allFiles
        .filter((file) => file.endsWith(".css"))
        .filter((file) => !isFileHeaderCorrect(file, HEADER_TS.trim()));

    const allIncorrectFiles = [
        ...tsFiles,
        ...htmlFiles,
        ...cssFiles,
        ...scssFiles
    ];

    if (allIncorrectFiles.length > 0) {
        console.error("Legal header is missing at:");
        allIncorrectFiles.forEach((file) => console.error("  " + file));
        console.log();
        throw new Error("Legal header is missing in " + allIncorrectFiles.length + " files");
    }
}

main()
    .catch((err) => {
        console.error(err);
        process.exit(2);
    });
