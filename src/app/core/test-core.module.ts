/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {NgModule, Optional, SkipSelf} from "@angular/core";
import data from "../../assets/config/config.json";
import {APP_CONFIGURATION, IAppConfiguration} from "./configuration";
import {CONTACT_DATA_BASE_ROUTE, HELP_ROUTE, NOMINATIM_ROUTE, PORTAL_ROUTE, SPA_BACKEND_ROUTE} from "./external-routes";

@NgModule({
    providers: [
        {provide: APP_CONFIGURATION, useValue: data},
        {provide: PORTAL_ROUTE, useFactory: (config: IAppConfiguration) => config.routes.portal, deps: [APP_CONFIGURATION]},
        {provide: SPA_BACKEND_ROUTE, useFactory: (config: IAppConfiguration) => config.routes.spaBackend, deps: [APP_CONFIGURATION]},
        {
            provide: CONTACT_DATA_BASE_ROUTE,
            useFactory: (config: IAppConfiguration) => config.routes.contactDataBase, deps: [APP_CONFIGURATION]
        },
        {provide: NOMINATIM_ROUTE, useFactory: (config: IAppConfiguration) => config.nominatim.url, deps: [APP_CONFIGURATION]},
        {provide: HELP_ROUTE, useFactory: (config: IAppConfiguration) => config.routes.userDocu, deps: [APP_CONFIGURATION]}
    ]
})
export class TestCoreModule {

    constructor(@Optional() @SkipSelf() duplicate: TestCoreModule) {
        if (duplicate != null) {
            throw new Error("TestCoreModule is imported multiple times");
        }
    }
}
