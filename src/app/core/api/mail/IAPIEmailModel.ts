/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPIEmailAttachmentModel} from "./IAPIEmailAttachmentModel";

/**
 * Interface which represents the model of an email. Contains meta data and mail content as text or html.
 */
export interface IAPIEmailModel {
    identifier: string;
    subject: string;
    date: string;
    from: string;
    textPlain: string;
    textHtml: string;
    attachments: IAPIEmailAttachmentModel[];
}
