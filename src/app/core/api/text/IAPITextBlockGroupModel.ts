/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPITextBlockModel} from "./IAPITextBlockModel";

/**
 * Interface which represents a single text block template group.
 */
export interface IAPITextBlockGroupModel {

    /**
     * Name of the group.
     */
    groupName: string;

    /**
     * All available text block templates in the group.
     */
    textBlocks: IAPITextBlockModel[];

}
