/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/**
 * Interface which represents the model of the statement history as saved in the back end database.
 */
export interface IAPIStatementHistory {
    processName: string;
    processVersion: number;
    finishedProcessActivities: IAPIProcessActivity[];
    currentProcessActivities: IAPIProcessActivity[];
}

/**
 * Interface which represents the model of a single process activity.
 */
export interface IAPIProcessActivity {
    id: string;
    activityId: string;
    activityName: string;
    activityType: string;
    assignee: string;
    startTime: string;
    endTime: string;
    durationInMillis: number;
    processDefinitionKey: string;
    processDefinitionId: string;
    canceled: boolean;
    completeScope: boolean;
    tenantId: string;
}
