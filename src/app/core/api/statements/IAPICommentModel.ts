/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/**
 * Model for a user comment in the back end data base
 */
export interface IAPICommentModel {

    id: number;

    text: string;

    userName: string;

    firstName: string;

    lastName: string;

    timestamp: string;

    editable: boolean;

}
