/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {HttpClient, HttpClientModule} from "@angular/common/http";
import {APP_INITIALIZER, NgModule} from "@angular/core";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import {I18nService} from "./i18n.service";

export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, "./assets/i18n/", ".i18.json");
}

export function i18nInitializer(i18nService: I18nService) {
    return () => Promise.resolve()
        .then(() => i18nService.initialize())
        .catch((err) => console.error(err));
}

@NgModule({
    imports: [
        HttpClientModule,
        TranslateModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient]
            },
            defaultLanguage: "de",
            useDefaultLang: true
        }),
    ],
    exports: [
        TranslateModule
    ],
    providers: [
        {
            provide: APP_INITIALIZER,
            useFactory: i18nInitializer,
            deps: [I18nService],
            multi: true
        }
    ]
})
export class I18nModule {

}
