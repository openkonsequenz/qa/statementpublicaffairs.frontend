/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {HttpErrorResponse, HttpRequest} from "@angular/common/http";
import {TestBed} from "@angular/core/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {EHttpStatusCodes} from "../../util/http";
import {SPA_BACKEND_ROUTE} from "../external-routes";
import {AuthInterceptorService} from "./auth-interceptor.service";
import {AuthService} from "./auth.service";

describe("AuthInterceptorService", () => {

    let service: AuthInterceptorService;

    const mockBaseValue = "/base-url";
    const mockAuthService = {
        token: "sometoken",
        clear: () => {
        }
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule],
            providers: [
                {provide: SPA_BACKEND_ROUTE, useValue: mockBaseValue},
                {provide: AuthService, useValue: mockAuthService}
            ]
        });
        service = TestBed.inject(AuthInterceptorService);
    });

    describe("needsAuthorization", () => {
        it("should return false for an unknown base url", () => {
            const needsAuthorizationResult = service.needsAuthorization("/any-random/url");
            expect(needsAuthorizationResult).toBeFalse();
        });

        it("should return false for the specified unauthorized endpoints", () => {
            const needsAuthorizationResult = service.needsAuthorization(`${mockBaseValue}/version`);
            expect(needsAuthorizationResult).toBeFalse();
        });

        it("should return true for any url starting with the base url and not one of the unauthorized endpoints", () => {
            let needsAuthorizationResult = service.needsAuthorization(`${mockBaseValue}/any-endpoint-here`);
            expect(needsAuthorizationResult).toBeTrue();
            needsAuthorizationResult = service.needsAuthorization(`${mockBaseValue}/another-endpoint`);
            expect(needsAuthorizationResult).toBeTrue();
        });
    });

    describe("addToken", () => {
        beforeEach(() => {
            mockAuthService.token = "sometoken";
        });

        it("should add the token to the http request headers", () => {
            const request = new HttpRequest("POST", "url", null);
            const newRequest = service.addToken(request);
            const authHeaderValue = newRequest.headers.get("Authorization");
            expect(authHeaderValue).toBeTruthy();
            expect(authHeaderValue).toEqual(`Bearer ${mockAuthService.token}`);
        });

        it("should not change anything about the request if no token is set", () => {
            const request = new HttpRequest("POST", "url", null);
            mockAuthService.token = null;
            const newRequest = service.addToken(request);
            expect(newRequest).toBe(request);
        });
    });

    describe("onUnauthorized", () => {
        it("should call AuthService.clear to delete the token", () => {
            spyOn(mockAuthService, "clear");
            const errorResponse = new HttpErrorResponse({status: EHttpStatusCodes.UNAUTHORIZED});
            service.onUnauthorized(errorResponse);
            expect(mockAuthService.clear).toHaveBeenCalled();
        });

        it("should put the response into the unquthorized$ observable", () => {
            let valueReceived = false;
            const unauthorizedError = service.unauthorized$.subscribe(() => valueReceived = true);
            const errorResponse = new HttpErrorResponse({status: EHttpStatusCodes.UNAUTHORIZED});
            service.onUnauthorized(errorResponse);
            unauthorizedError.unsubscribe();
            expect(valueReceived).toBeTrue();
        });
    });
});





