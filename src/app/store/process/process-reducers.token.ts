/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {InjectionToken} from "@angular/core";
import {ActionReducerMap} from "@ngrx/store";
import {IProcessStoreState} from "./model";
import {diagramReducer, historyReducer, processLoadingReducer, statementTaskReducer, tasksReducer} from "./reducers";
import {claimDetailsReducer} from "./reducers/claim-details.reducer";

export const PROCESS_FEATURE_NAME = "process";

export const PROCESS_REDUCER = new InjectionToken<ActionReducerMap<IProcessStoreState>>("Statements store reducer", {
    providedIn: "root",
    factory: () => ({
        diagram: diagramReducer,
        history: historyReducer,
        statementTasks: statementTaskReducer,
        tasks: tasksReducer,
        claimDetails: claimDetailsReducer,
        loading: processLoadingReducer
    })
});
