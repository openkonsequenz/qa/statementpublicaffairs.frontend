/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPIProcessTask, IAPIStatementHistory} from "../../../core";
import {IAPIClaimDetails} from "../../../core/api/process/IAPIClaimDetails";
import {TStoreEntities} from "../../../util/store";

export interface IProcessStoreState {

    statementTasks: TStoreEntities<string[]>;

    history: TStoreEntities<IAPIStatementHistory>;

    diagram: TStoreEntities<string>;

    tasks: TStoreEntities<IAPIProcessTask>;

    claimDetails: TStoreEntities<IAPIClaimDetails>;

    loading?: boolean;

}
