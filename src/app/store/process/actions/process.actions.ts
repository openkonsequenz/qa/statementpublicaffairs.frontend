/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Action, createAction, props} from "@ngrx/store";
import {EAPIProcessTaskDefinitionKey, IAPIProcessObject, IAPIProcessTask, IAPIStatementHistory, TCompleteTaskVariable} from "../../../core";
import {IAPIClaimDetails} from "../../../core/api/process/IAPIClaimDetails";

export const claimTaskAction = createAction(
    "[Details] Claim task",
    props<{ statementId: number; taskId: string; options?: { negative?: boolean } }>()
);

export const cancelStatementAction = createAction(
    "[API] Cancel/Delete an active statement",
    props<{ statementId: number }>()
);

export const claimAndCompleteTask = createAction(
    "[Details] Claim and complete Task",
    props<{
        statementId: number;
        taskId: string;
        assignee: string;
        variables: TCompleteTaskVariable;
        claimNext?: boolean | EAPIProcessTaskDefinitionKey;
    }>()
);

export const unclaimAllTasksAction = createAction(
    "[Details/Edit] Unclaim all tasks",
    props<{ statementId: number; assignee: string }>()
);


export const completeTaskAction = createAction(
    "[Edit] Complete task",
    props<{
        statementId: number;
        taskId: string;
        variables: IAPIProcessObject;
        claimNext?: boolean | EAPIProcessTaskDefinitionKey;
        endWith?: Action[];
    }>()
);

export const fetchClaimDetailsAction = createAction(
    "[API] Fetch claim details",
    props<{
        statementId: number;
        taskId: string;
    }>()
);

export const setClaimDetailsAction = createAction(
    "[API] Set claim details",
    props<{
        claimDetails: IAPIClaimDetails;
    }>()
);

export const deleteClaimDetailsAction = createAction(
    "[Store] Delete claim details",
    props<{
        taskId: string;
    }>()
);

export const setStatementTasksAction = createAction(
    "[API] Set statement tasks",
    props<{ statementId: number; tasks: IAPIProcessTask[] }>()
);

export const setTaskEntityAction = createAction(
    "[API] Set task entity",
    props<{ task: IAPIProcessTask }>()
);

export const deleteTaskAction = createAction(
    "[API] Delete task",
    props<{ statementId: number; taskId?: string }>()
);

export const setHistoryAction = createAction(
    "[API] Set process history",
    props<{ statementId: number; history: IAPIStatementHistory }>()
);

export const setDiagramAction = createAction(
    "[API] Set bpmn diagram",
    props<{ statementId: number; diagram: string }>()
);

export const setProcessLoadingAction = createAction(
    "[API] Set process loading",
    props<{ statementId: number; loading: boolean }>()
);
