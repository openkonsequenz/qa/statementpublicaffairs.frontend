/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {setProcessLoadingAction} from "../actions";
import {processLoadingReducer} from "./process-loading.reducer";

describe("processLoadingReducer", () => {

    it("should toggle loading state", () => {
        let initialState = false;
        let action = setProcessLoadingAction({statementId: 19, loading: true});
        expect(processLoadingReducer(initialState, action)).toBe(true);

        initialState = true;
        action = setProcessLoadingAction({statementId: 19, loading: false});
        expect(processLoadingReducer(initialState, action)).toBe(false);
    });

});
