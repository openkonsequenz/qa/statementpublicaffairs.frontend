/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPIStatementHistory} from "../../../core/api/process";
import {TStoreEntities} from "../../../util/store";
import {setHistoryAction} from "../actions";
import {historyReducer} from "./history.reducer";

describe("historyReducer", () => {

    const history: IAPIStatementHistory = {
        processName: "processName",
        processVersion: 1,
        finishedProcessActivities: [],
        currentProcessActivities: []
    };

    it("should return the previous state if not supplied with a statementid", () => {
        const initialState: TStoreEntities<IAPIStatementHistory> = {};
        let action = setHistoryAction(undefined);
        let state = historyReducer(initialState, action);
        expect(state).toEqual(initialState);

        action = setHistoryAction({statementId: null, history});
        state = historyReducer(initialState, action);
        expect(state).toEqual(initialState);
    });

    it("should set the history data for the given statementid", () => {
        let initialState: TStoreEntities<IAPIStatementHistory> = {};
        let action = setHistoryAction({statementId: 1, history});
        let state = historyReducer(initialState, action);
        expect(state).toEqual({1: history});

        initialState = state;
        const anotherHistory = {...history, processName: "anotherProcessName"};
        action = setHistoryAction({statementId: 2, history: anotherHistory});
        state = historyReducer(initialState, action);
        expect(state).toEqual({1: history, 2: anotherHistory});

        initialState = state;
        action = setHistoryAction({statementId: 2, history});
        state = historyReducer(initialState, action);
        expect(state).toEqual({1: history, 2: history});
    });

});
