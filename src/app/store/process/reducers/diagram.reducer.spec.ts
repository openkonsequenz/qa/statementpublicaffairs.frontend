/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {TStoreEntities} from "../../../util/store";
import {setDiagramAction} from "../actions";
import {diagramReducer} from "./diagram.reducer";

describe("diagramReducer", () => {

    it("should return the previous state if not supplied with a statementid", () => {
        const initialState: TStoreEntities<string> = {};
        let action = setDiagramAction(undefined);
        let state = diagramReducer(initialState, action);
        expect(state).toEqual(initialState);

        action = setDiagramAction({statementId: null, diagram: "string"});
        state = diagramReducer(initialState, action);
        expect(state).toEqual(initialState);
    });

    it("should set the diagram for the given statementid", () => {
        let initialState: TStoreEntities<string> = {};
        let action = setDiagramAction({statementId: 1, diagram: "string"});
        let state = diagramReducer(initialState, action);
        expect(state).toEqual({1: "string"});

        initialState = state;
        action = setDiagramAction({statementId: 2, diagram: "string2"});
        state = diagramReducer(initialState, action);
        expect(state).toEqual({1: "string", 2: "string2"});

        initialState = state;
        action = setDiagramAction({statementId: 2, diagram: "another string"});
        state = diagramReducer(initialState, action);
        expect(state).toEqual({1: "string", 2: "another string"});
    });

});
