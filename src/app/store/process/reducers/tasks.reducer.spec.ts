/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {EAPIProcessTaskDefinitionKey, IAPIProcessTask} from "../../../core/api/process";
import {TStoreEntities} from "../../../util/store";
import {deleteTaskAction, setStatementTasksAction, setTaskEntityAction} from "../actions";
import {tasksReducer} from "./tasks.reducer";

describe("tasksReducer", () => {

    it("should set tasks to the given statementid", () => {

        const actionPayload = {statementId: "1"} as unknown as { statementId: number; tasks: IAPIProcessTask[] };
        let initialState: TStoreEntities<IAPIProcessTask> = {};
        let action = setStatementTasksAction(actionPayload);
        let state = tasksReducer(initialState, action);
        expect(state).toEqual(initialState);

        action = setStatementTasksAction(undefined);
        state = tasksReducer(initialState, action);
        expect(state).toEqual(initialState);

        actionPayload.statementId = 1;
        actionPayload.tasks = [
            {
                statementId: 1,
                taskId: "taskId",
                taskDefinitionKey: EAPIProcessTaskDefinitionKey.ADD_BASIC_INFO_DATA,
                processDefinitionKey: "processDefinitionKey",
                assignee: "assignee",
                authorized: true,
                requiredVariables: {}
            },
            {
                statementId: 2,
                taskId: "taskId2",
                taskDefinitionKey: EAPIProcessTaskDefinitionKey.ADD_BASIC_INFO_DATA,
                processDefinitionKey: "processDefinitionKey",
                assignee: "assignee",
                authorized: true,
                requiredVariables: {}
            }
        ] as IAPIProcessTask[];
        action = setStatementTasksAction(actionPayload);
        state = tasksReducer(initialState, action);
        expect(state).toEqual({taskId: actionPayload.tasks[0]});

        initialState = state;
        actionPayload.statementId = 2;
        action = setStatementTasksAction(actionPayload);
        state = tasksReducer(initialState, action);
        expect(state).toEqual({taskId: actionPayload.tasks[0], taskId2: actionPayload.tasks[1]});

        initialState = state;
        actionPayload.statementId = 3;
        action = setStatementTasksAction(actionPayload);
        state = tasksReducer(initialState, action);
        expect(state).toEqual(initialState);

        initialState = state;
        actionPayload.statementId = 1;
        actionPayload.tasks[0].taskId = "changedTaskId";
        action = setStatementTasksAction(actionPayload);
        state = tasksReducer(initialState, action);
        expect(state).toEqual({changedTaskId: actionPayload.tasks[0], taskId2: actionPayload.tasks[1]});
    });

    it("should delete a task for a given taskId from the state", () => {

        const actionPayload = {statementId: "1", taskId: "taskId"} as unknown as { statementId: number; taskId: string };
        const initialState: TStoreEntities<IAPIProcessTask> = {
            taskId: {
                statementId: 1,
                taskId: "taskId",
                taskDefinitionKey: EAPIProcessTaskDefinitionKey.ADD_BASIC_INFO_DATA,
                processDefinitionKey: "processDefinitionKey",
                assignee: "assignee",
                authorized: true,
                requiredVariables: {}
            }
        };

        let action = deleteTaskAction(actionPayload);
        let state = tasksReducer(initialState, action);
        expect(state).toEqual(initialState);

        actionPayload.statementId = 1;
        actionPayload.taskId = {} as string;
        action = deleteTaskAction(actionPayload);
        state = tasksReducer(initialState, action);
        expect(state).toEqual(initialState);

        actionPayload.statementId = 1;
        actionPayload.taskId = "taskId";
        action = deleteTaskAction(actionPayload);
        state = tasksReducer(initialState, action);
        expect(state).toEqual({});
    });

    it("should update a given task in the state", () => {
        const actionPayload = {task: null} as unknown as { task: IAPIProcessTask };
        const initialState: TStoreEntities<IAPIProcessTask> = {
            taskId: {
                statementId: 1,
                taskId: "taskId",
                taskDefinitionKey: EAPIProcessTaskDefinitionKey.ADD_BASIC_INFO_DATA,
                processDefinitionKey: "processDefinitionKey",
                assignee: "assignee",
                authorized: true,
                requiredVariables: {}
            }
        };

        let action = setTaskEntityAction(actionPayload);
        let state = tasksReducer(initialState, action);
        expect(state).toEqual(initialState);

        actionPayload.task = {taskId: "taskId", assignee: "changedValue"} as IAPIProcessTask;
        action = setTaskEntityAction(actionPayload);
        state = tasksReducer(initialState, action);
        expect(state).toEqual({
            taskId: {
                statementId: 1,
                taskId: "taskId",
                taskDefinitionKey: EAPIProcessTaskDefinitionKey.ADD_BASIC_INFO_DATA,
                processDefinitionKey: "processDefinitionKey",
                assignee: "changedValue",
                authorized: true,
                requiredVariables: {}
            }
        });
    });

});
