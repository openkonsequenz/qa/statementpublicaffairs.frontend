/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {NgModule} from "@angular/core";
import {EffectsModule} from "@ngrx/effects";
import {StoreModule} from "@ngrx/store";
import {ProcessTaskEffect} from "./effects";
import {PROCESS_FEATURE_NAME, PROCESS_REDUCER} from "./process-reducers.token";

@NgModule({
    imports: [
        StoreModule.forFeature(PROCESS_FEATURE_NAME, PROCESS_REDUCER),
        EffectsModule.forFeature([
            ProcessTaskEffect
        ])
    ]
})
export class ProcessStoreModule {

}
