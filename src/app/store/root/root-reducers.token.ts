/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {InjectionToken} from "@angular/core";
import {ActionReducerMap} from "@ngrx/store";
import {IRootStoreState} from "./model";
import {exitCodeReducer, isLoadingReducer, queryParamsReducer, userReducer, versionBackEndReducer, versionReducer} from "./reducers";
import {errorCodeReducer} from "./reducers/error-code.reducer";

export const ROOT_REDUCER = new InjectionToken<ActionReducerMap<IRootStoreState>>("Root store reducer", {
    providedIn: "root",
    factory: () => ({
        version: versionReducer,
        versionBackEnd: versionBackEndReducer,
        user: userReducer,
        isLoading: isLoadingReducer,
        exitCode: exitCodeReducer,
        queryParams: queryParamsReducer,
        error: errorCodeReducer
    })
});
