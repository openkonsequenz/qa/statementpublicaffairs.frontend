/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Params} from "@angular/router";
import {IAPIUserInfo} from "../../../core/api/core";
import {EExitCode} from "./EExitCode";

export interface IRootStoreState {
    /**
     * Is true if initialization has finished
     */
    isLoading?: boolean;

    /**
     * Version of front End
     */
    version?: string;

    /**
     * Version of back end
     */
    versionBackEnd?: string;

    user?: IAPIUserInfo;

    /**
     * Fatal error which traps the user on the error page
     */
    exitCode?: EExitCode;

    /**
     * Query parameters extracted from the activated route, i.e. current url
     */
    queryParams?: Params;

    /**
     * Error that's received from any outgoing requests.
     */
    error?: string;
}
