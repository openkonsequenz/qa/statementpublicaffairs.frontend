/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createAction, props} from "@ngrx/store";
import {IAPIUserInfo, IAPIVersion} from "../../../core";
import {TStoreEntities} from "../../../util";
import {EExitCode} from "../model";

export const intializeAction = createAction(
    "[Store] Initialize"
);

export const completeInitializationAction = createAction(
    "[Store] Complete initialization"
);

export const toggleLoadingPageAction = createAction(
    "[Store] Toggle loading page",
    props<{ isLoading?: boolean }>()
);

export const keepSessionAliveAction = createAction(
    "[Store] Keep session alive"
);

export const fetchVersionAction = createAction(
    "[Store] Fetch version"
);

export const setBackEndVersionAction = createAction(
    "[API] Set back end version",
    props<IAPIVersion>()
);

export const setUserAction = createAction(
    "[API] Set user",
    props<{ user: IAPIUserInfo }>()
);

export const clearUserAction = createAction(
    "[API] Clear user"
);

export const openExitPageAction = createAction(
    "[API] Open exit page",
    props<{ code: EExitCode }>()
);

export const logOutAction = createAction(
    "[NavigationFrame] Log out"
);

export const setQueryParamsAction = createAction(
    "[Router] Set query params",
    props<{ queryParams: TStoreEntities<any> }>()
);


export const openContactDataBaseAction = createAction(
    "[Details/Edit] Open contact data base in new tab"
);

export const openFileAction = createAction(
    "[API] Open file in new tab",
    props<{ file: File }>()
);
