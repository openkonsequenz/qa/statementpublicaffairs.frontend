/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/


import {IAPIPaginationResponse} from "../../../../core";
import {TStoreEntities} from "../../../../util";
import {setContactEntityAction, setContactsSearchAction} from "../../actions";
import {IContactEntity} from "../../model";
import {contactEntitiesReducer} from "./contact-entities.reducer";

describe("contactEntitiesReducer", () => {

    it("should not change the state on setContactEntityAction with no values given", () => {
        const initialState: TStoreEntities<IContactEntity> = {};
        let action = setContactEntityAction(undefined);
        let state = contactEntitiesReducer(initialState, action);
        expect(state).toEqual(initialState);

        action = setContactEntityAction({entity: undefined});
        state = contactEntitiesReducer(initialState, action);
        expect(state).toEqual(initialState);

        action = setContactEntityAction({entity: null});
        state = contactEntitiesReducer(initialState, action);
        expect(state).toEqual(initialState);
    });

    it("should add the entity only if it has an id property", () => {
        const initialState: TStoreEntities<IContactEntity> = {};
        let action = setContactEntityAction({entity: {firstName: "firstName"}});
        let state = contactEntitiesReducer(initialState, action);
        expect(state).toEqual(initialState);

        action = setContactEntityAction({entity: {id: "1", firstName: "firstName"}});
        state = contactEntitiesReducer(initialState, action);
        const newState = {};
        newState["1"] = {id: "1", firstName: "firstName"};
        expect(state).toEqual(newState);
    });

    it("should not change the state on setContactsSearchAction with no values given", () => {
        const initialState: TStoreEntities<IContactEntity> = {};
        let action = setContactsSearchAction(undefined);
        let state = contactEntitiesReducer(initialState, action);
        expect(state).toEqual(initialState);

        let response = {content: null} as IAPIPaginationResponse<Partial<IContactEntity>>;
        action = setContactsSearchAction({results: response});
        state = contactEntitiesReducer(initialState, action);
        expect(state).toEqual(initialState);

        response = {content: []} as IAPIPaginationResponse<Partial<IContactEntity>>;
        action = setContactsSearchAction({results: response});
        state = contactEntitiesReducer(initialState, action);
        expect(state).toEqual(initialState);
    });

    it("should add the entities only if it has an id property", () => {
        const initialState: TStoreEntities<IContactEntity> = {};
        let response = {
            content: [
                {
                    firstName: "firstName"
                }
            ]
        } as IAPIPaginationResponse<Partial<IContactEntity>>;
        let action = setContactsSearchAction({results: response});
        let state = contactEntitiesReducer(initialState, action);
        expect(state).toEqual(initialState);

        response = {
            content: [
                {
                    id: "1",
                    firstName: "firstName"
                }
            ]
        } as IAPIPaginationResponse<Partial<IContactEntity>>;
        action = setContactsSearchAction({results: response});
        state = contactEntitiesReducer(initialState, action);
        let newState = {};
        newState["1"] = {id: "1", firstName: "firstName"};
        expect(state).toEqual(newState);

        response = {
            content: [
                {
                    id: "1",
                    firstName: "firstName"
                },
                {
                    lastName: "lastName"
                }
            ]
        } as IAPIPaginationResponse<Partial<IContactEntity>>;
        action = setContactsSearchAction({results: response});
        state = contactEntitiesReducer(initialState, action);
        newState = {};
        newState["1"] = {id: "1", firstName: "firstName"};
        expect(state).toEqual(newState);
    });

});
