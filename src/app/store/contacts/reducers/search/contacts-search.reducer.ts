/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createReducer, on} from "@ngrx/store";
import {IAPIPaginationResponse} from "../../../../core";
import {arrayJoin} from "../../../../util";
import {setContactsSearchAction} from "../../actions";

export const contactsSearchReducer = createReducer<IAPIPaginationResponse<string>>(
    undefined,
    on(setContactsSearchAction, (state, payload) => payload.results == null ? undefined : {
        ...payload.results,
        content: arrayJoin(payload.results.content)
            .filter((_) => _.id != null)
            .map((_) => _.id)
    })
);
