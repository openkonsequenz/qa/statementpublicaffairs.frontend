/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {FormControl, FormGroup, ValidatorFn, Validators} from "@angular/forms";
import {IAPIPartialStatementModel} from "../../../../core/api/statements";
import {createFormGroup} from "../../../../util/forms";
import {createAttachmentForm, IAttachmentFormValue} from "../../../attachments/model";

export interface IStatementInformationFormValue extends IAPIPartialStatementModel {

    attachments?: IAttachmentFormValue;

}

export function createStatementInformationForm() {
    const dueDateValidator: ValidatorFn = (formGroup: FormGroup) => {
        const dueDate = formGroup.get("dueDate").value;
        const departmentsDueDate = formGroup.get("departmentsDueDate").value;
        return departmentsDueDate <= dueDate
            ? null
            : {departmentsDueDateInvalid: true};
    };

    return createFormGroup<IStatementInformationFormValue>({
        title: new FormControl(undefined, [Validators.required]),
        creationDate: new FormControl(undefined, [Validators.required]),
        dueDate: new FormControl(undefined, [Validators.required]),
        departmentsDueDate: new FormControl(undefined, [Validators.required]),
        receiptDate: new FormControl(undefined, [Validators.required]),
        typeId: new FormControl(undefined, [Validators.required]),
        city: new FormControl(undefined, [Validators.required]),
        district: new FormControl(undefined, [Validators.required]),
        contactId: new FormControl(undefined, [Validators.required]),
        sourceMailId: new FormControl(null),
        customerReference: new FormControl(),
        attachments: createAttachmentForm()
    }, {validators: dueDateValidator});
}
