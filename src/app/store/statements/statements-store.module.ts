/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {NgModule} from "@angular/core";
import {EffectsModule} from "@ngrx/effects";
import {StoreModule} from "@ngrx/store";
import {
    CommentsEffect,
    CompileStatementArrangementEffect,
    FetchStatementDetailsEffect,
    FetchTextArrangementEffect,
    SearchStatementPositionsEffect,
    SearchStatementsEffect,
    SubmitStatementInformationFormEffect,
    SubmitWorkflowFormEffect,
    ValidateStatementArrangementEffect
} from "./effects";
import {FetchDashboardStatementsEffect} from "./effects/fetch-dashboard-statements";
import {FetchStatementTextblockHistoryEffect} from "./effects/fetch-statement-history";
import {SubmitStatementEditorFormEffect} from "./effects/submit-statement-editor-form";
import {STATEMENTS_NAME, STATEMENTS_REDUCER} from "./statements-reducers.token";

@NgModule({
    imports: [
        StoreModule.forFeature(STATEMENTS_NAME, STATEMENTS_REDUCER),
        EffectsModule.forFeature([
            CommentsEffect,
            CompileStatementArrangementEffect,
            FetchStatementDetailsEffect,
            FetchTextArrangementEffect,
            SearchStatementsEffect,
            SearchStatementPositionsEffect,
            SubmitStatementEditorFormEffect,
            SubmitStatementInformationFormEffect,
            SubmitWorkflowFormEffect,
            ValidateStatementArrangementEffect,
            FetchDashboardStatementsEffect,
            FetchStatementTextblockHistoryEffect
        ])
    ]
})
export class StatementsStoreModule {

}
