/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPISearchOptions} from "../../../../core";
import {createPaginationResponseMock, createStatementModelMock} from "../../../../test";
import {setStatementSearchResultAction, startStatementSearchAction} from "../../actions";
import {statementSearchReducer} from "./statement-search.reducer";

describe("statementSearchReducer", () => {

    it("should not affect state on start search action ", () => {
        const initialState = createPaginationResponseMock([0]);
        const options: IAPISearchOptions = {q: ""};
        const action = startStatementSearchAction({options});
        const state = statementSearchReducer(initialState, action);
        expect(state).toBe(initialState);
    });

    it("should set search results on set search result action", () => {
        const results = createPaginationResponseMock([createStatementModelMock(18), createStatementModelMock(19)]);
        const search = createPaginationResponseMock([18, 19]);
        const action = setStatementSearchResultAction({results});

        let state = statementSearchReducer(undefined, action);
        expect(state).toEqual(search);

        results.content.push(null, createStatementModelMock(null));
        state = statementSearchReducer(undefined, action);
        expect(state).toEqual(search);

        results.content = null;
        search.content = [];
        state = statementSearchReducer(undefined, action);
        expect(state).toEqual(search);

        state = statementSearchReducer(undefined, setStatementSearchResultAction({results: undefined}));
        expect(state).toEqual(undefined);
    });

});
