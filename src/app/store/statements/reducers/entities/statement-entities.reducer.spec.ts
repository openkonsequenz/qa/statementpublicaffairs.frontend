/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Action} from "@ngrx/store";
import {createStatementModelMock, createWorkflowDataMock} from "../../../../test";
import {TStoreEntities} from "../../../../util/store";
import {fetchStatementDetailsAction, updateStatementEntityAction, updateStatementInfoAction} from "../../actions";
import {IStatementEntity} from "../../model";
import {statementEntitiesReducer} from "./statement-entities.reducer";

describe("statementEntitiesReducer", () => {

    it("should not affect state on fetch action", () => {
        const initialState: TStoreEntities<IStatementEntity> = {};
        const action = fetchStatementDetailsAction({statementId: 19});
        const state = statementEntitiesReducer(initialState, action);
        expect(state).toBe(initialState);
    });

    it("should create new entity of statement on set action", () => {
        const initialState: TStoreEntities<IStatementEntity> = {};
        const entity: IStatementEntity = {info: createStatementModelMock(19)};
        const action = updateStatementEntityAction({statementId: 19, entity});
        const state = statementEntitiesReducer(initialState, action);
        expect(state).toEqual({
            19: {
                info: createStatementModelMock(19)
            }
        });
    });

    it("should update entity of statement on set action", () => {
        const info = createStatementModelMock(19);
        const workflow = createWorkflowDataMock();
        const initialState: TStoreEntities<IStatementEntity> = {18: {}, 19: {workflow}};
        const entity: IStatementEntity = {info};
        const action = updateStatementEntityAction({statementId: 19, entity});
        const state = statementEntitiesReducer(initialState, action);
        expect(state).toEqual({
            18: {},
            19: {info, workflow}
        });
    });

    it("should update info for list of statements", () => {
        const workflow = createWorkflowDataMock();
        const initialState: TStoreEntities<IStatementEntity> = {18: {}, 19: {workflow}};
        const action = updateStatementInfoAction({items: [createStatementModelMock(18), createStatementModelMock(19)]});
        const state = statementEntitiesReducer(initialState, action);
        expect(state).toEqual({
            18: {info: createStatementModelMock(18)},
            19: {info: createStatementModelMock(19), workflow}
        });
    });

    it("should not change state without any id", () => {
        const initialState: TStoreEntities<IStatementEntity> = {18: {}, 19: {workflow: createWorkflowDataMock()}};
        let action: Action = updateStatementEntityAction({statementId: undefined, entity: {}});
        let state = statementEntitiesReducer(initialState, action);
        expect(state).toBe(initialState);
        action = updateStatementInfoAction({items: null});
        state = statementEntitiesReducer(initialState, action);
        expect(state).toBe(initialState);
        action = updateStatementInfoAction({items: [null, createStatementModelMock(null)]});
        state = statementEntitiesReducer(initialState, action);
        expect(state).toBe(initialState);
    });

});
