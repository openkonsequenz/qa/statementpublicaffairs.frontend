/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createSelector} from "@ngrx/store";
import {selectEntityWithIdProjector, selectPropertyProjector} from "../../../util/store";
import {queryParamsIdSelector} from "../../root/selectors";
import {settingsStoreSelector} from "../../settings/selectors";
import {statementsStoreStateSelector} from "./statements-store-state.selectors";

export const statementConfigurationEntitiesSelector = createSelector(
    statementsStoreStateSelector,
    selectPropertyProjector("configuration", {})
);

export const statementConfigurationSelector = createSelector(
    statementConfigurationEntitiesSelector,
    queryParamsIdSelector,
    selectEntityWithIdProjector()
);

export const getStatementDepartmentConfigurationSelector = createSelector(
    statementConfigurationSelector,
    selectPropertyProjector("departments")
);

export const getStatementSectorsSelector = createSelector(
    settingsStoreSelector,
    statementConfigurationSelector,
    (settings, configuration) => configuration?.sectors ? configuration.sectors : settings?.sectors
);

export const getStatementTextConfigurationSelector = createSelector(
    statementConfigurationSelector,
    selectPropertyProjector("text")
);
