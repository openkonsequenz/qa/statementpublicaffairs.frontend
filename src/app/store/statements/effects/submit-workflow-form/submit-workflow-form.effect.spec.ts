/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {TestBed} from "@angular/core/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {provideMockActions} from "@ngrx/effects/testing";
import {Action} from "@ngrx/store";
import {EMPTY, Observable, of, Subscription} from "rxjs";
import {IAPIWorkflowData, SPA_BACKEND_ROUTE} from "../../../../core";
import {ProcessTaskEffect} from "../../../process/effects";
import {setStatementLoadingAction, submitWorkflowDataFormAction, updateStatementEntityAction} from "../../actions";
import {IWorkflowFormValue} from "../../model";
import {SubmitWorkflowFormEffect} from "./submit-workflow-form.effect";

describe("SubmitWorkflowFormEffect", () => {

    let actions$: Observable<Action>;
    let httpTestingController: HttpTestingController;
    let effect: SubmitWorkflowFormEffect;
    let subscription: Subscription;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule,
                RouterTestingModule
            ],
            providers: [
                SubmitWorkflowFormEffect,
                provideMockActions(() => actions$),
                {
                    provide: SPA_BACKEND_ROUTE,
                    useValue: "/"
                }
            ]
        });
        effect = TestBed.inject(SubmitWorkflowFormEffect);
        httpTestingController = TestBed.inject(HttpTestingController);
    });

    afterEach(() => {
        if (subscription != null) {
            subscription.unsubscribe();
        }
    });

    it("should submit workflow form data to back end without completing task", () => {
        const results: Action[] = [];
        const statementId = 19;
        const taskId = "1919";
        const parentIds = [18, 19];
        const data = createFormValue(parentIds);
        const expectedData = createData();

        actions$ = of(submitWorkflowDataFormAction({statementId, taskId, data, completeTask: false}));
        subscription = effect.submit$.subscribe((action) => results.push(action));

        expectPostWorkflowRequest(statementId, taskId, expectedData);
        expectPostParentIdsRequest(statementId, taskId, parentIds);
        subscription.unsubscribe();

        expect(results).toEqual([
            setStatementLoadingAction({loading: {submittingWorkflowData: true}}),
            updateStatementEntityAction({statementId, entity: {workflow: expectedData}}),
            updateStatementEntityAction({statementId, entity: {parentIds}}),
            setStatementLoadingAction({loading: {submittingWorkflowData: false}})
        ]);

        httpTestingController.verify();
    });

    it("should submit workflow form data to back end with completing task", () => {
        const results: Action[] = [];
        const statementId = 19;
        const taskId = "1919";
        const parentIds = [18, 19];
        const data = createFormValue(parentIds);
        const expectedData = createData();
        const completeTaskSpy = spyOn(TestBed.inject(ProcessTaskEffect), "completeTask").and.returnValue(EMPTY);

        actions$ = of(submitWorkflowDataFormAction({statementId, taskId, data, completeTask: true}));
        subscription = effect.submit$.subscribe((action) => results.push(action));

        expectPostWorkflowRequest(statementId, taskId, expectedData);
        expectPostParentIdsRequest(statementId, taskId, parentIds);

        expect(results).toEqual([
            setStatementLoadingAction({loading: {submittingWorkflowData: true}}),
            updateStatementEntityAction({statementId, entity: {workflow: expectedData}}),
            updateStatementEntityAction({statementId, entity: {parentIds}}),
            setStatementLoadingAction({loading: {submittingWorkflowData: false}})
        ]);
        expect(completeTaskSpy).toHaveBeenCalledWith(statementId, taskId, {}, true);

        httpTestingController.verify();
    });

    it("should not make API calls if action data is missing", () => {
        const data = {} as any;
        actions$ = of(...[
            submitWorkflowDataFormAction({statementId: null, taskId: "19", data, completeTask: true}),
            submitWorkflowDataFormAction({statementId: 19, taskId: null, data, completeTask: false}),
            submitWorkflowDataFormAction({statementId: 19, taskId: "19", data: null}),
        ]);
        subscription = effect.submit$.subscribe();
        expect(() => httpTestingController.verify()).not.toThrow();
    });

    function expectPostWorkflowRequest(statementId: number, taskId: string, body: IAPIWorkflowData) {
        const url = `/process/statements/${statementId}/task/${taskId}/workflow`;
        const request = httpTestingController.expectOne(url);
        expect(request.request.method).toBe("POST");
        expect(request.request.body).toEqual(body);
        request.flush(body);
    }

    function expectPostParentIdsRequest(statementId: number, taskId: string, parentIds: number[]) {
        const url = `/process/statements/${statementId}/task/${taskId}/workflow/parents`;
        const request = httpTestingController.expectOne(url);
        expect(request.request.method).toBe("POST");
        expect(request.request.body).toEqual(parentIds);
        request.flush(parentIds);
    }

    function createFormValue(parentIds: number[]): IWorkflowFormValue {
        return {
            departments: {
                selected: [
                    {
                        groupName: "Group A",
                        name: "Department 1"
                    },
                    {
                        groupName: "Group A",
                        name: "Department 2"
                    },
                    {
                        groupName: "Group B",
                        name: "Department 1"
                    }
                ],
                indeterminate: []
            }
            ,
            geographicPosition: "49.87627265513224,8.659071922302248,14",
            parentIds
        };
    }

    function createData(): IAPIWorkflowData {
        return {
            geoPosition: "49.87627265513224,8.659071922302248,14",
            mandatoryDepartments: {
                "Group A": ["Department 1", "Department 2"],
                "Group B": ["Department 1"]
            },
            optionalDepartments: {}
        };
    }

});

