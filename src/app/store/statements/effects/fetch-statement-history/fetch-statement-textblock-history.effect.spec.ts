/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {fakeAsync, TestBed} from "@angular/core/testing";
import {provideMockActions} from "@ngrx/effects/testing";
import {Action} from "@ngrx/store";
import {Observable, of, Subscription} from "rxjs";
import {IAPITextblockHistoryModel} from "../../../../core/api/statements/IAPITextblockHistoryModel";
import {IStoreTextblockHistoryVersionModel} from "../../../../core/api/statements/IAPITextblockHistoryVersionModel";
import {SPA_BACKEND_ROUTE} from "../../../../core/external-routes";
import {fetchStatementTextblockHistoryAction} from "../../actions";
import {FetchStatementTextblockHistoryEffect} from "./fetch-statement-textblock-history.effect";

describe("FetchStatementTextblockHistoryEffect", () => {

    let httpTestingController: HttpTestingController;
    let effect: FetchStatementTextblockHistoryEffect;
    let subscription: Subscription;
    let actions$: Observable<Action>;

    const returnValue: IAPITextblockHistoryModel = {
        statementId: 11,
        timestamp: "timestamp",
        currentVersion: "currentVersion",
        versionOrder: ["currentVersion"],
        versions: {
            currentVersion: {
            } as IStoreTextblockHistoryVersionModel
        }
    };

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                FetchStatementTextblockHistoryEffect,
                provideMockActions(() => actions$),
                {
                    provide: SPA_BACKEND_ROUTE,
                    useValue: "/"
                }
            ]
        });
        effect = TestBed.inject(FetchStatementTextblockHistoryEffect);
        httpTestingController = TestBed.inject(HttpTestingController);
    });

    afterEach(() => {
        if (subscription != null) {
            subscription.unsubscribe();
        }
    });

    it("should fetch textblock history from backend", fakeAsync(() => {

        const statementId = 11;
        const results: Action[] = [];

        actions$ = of(fetchStatementTextblockHistoryAction({statementId}));
        subscription = effect.fetchStatementTextblockHistory$.subscribe((action) => results.push(action));

        expectFetchStatementTextblockHistoryRequest(statementId);
    }));

    function expectFetchStatementTextblockHistoryRequest(statementId: number) {
        const url = `/statements/${statementId}/history`;
        const request = httpTestingController.expectOne(url);
        expect(request.request.method).toBe("GET");
        request.flush(returnValue);
    }

});
