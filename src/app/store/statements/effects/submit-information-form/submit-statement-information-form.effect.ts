/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Injectable} from "@angular/core";
import {Router} from "@angular/router";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {Action} from "@ngrx/store";
import {concat, defer, EMPTY, Observable, OperatorFunction, pipe} from "rxjs";
import {endWith, filter, map, startWith, switchMap, tap} from "rxjs/operators";
import {EAPIProcessTaskDefinitionKey, ProcessApiService, StatementsApiService} from "../../../../core";
import {shrinkString} from "../../../../util/forms";
import {catchHttpErrorTo, EHttpStatusCodes} from "../../../../util/http";
import {catchErrorTo, endWithObservable, ignoreError, throwAfterActionType} from "../../../../util/rxjs";
import {SubmitAttachmentsEffect} from "../../../attachments/effects/submit";
import {setTaskEntityAction} from "../../../process/actions";
import {ProcessTaskEffect} from "../../../process/effects";
import {setErrorAction} from "../../../root/actions";
import {EErrorCode} from "../../../root/model";
import {setStatementLoadingAction, submitStatementInformationFormAction, updateStatementEntityAction} from "../../actions";
import {IStatementInformationFormValue} from "../../model";

@Injectable({providedIn: "root"})
export class SubmitStatementInformationFormEffect {

    public readonly submit$ = createEffect(() => this.actions.pipe(
        ofType(submitStatementInformationFormAction),
        filter((action) => action.value != null),
        filter((action) => action.new || action.statementId != null && action.taskId != null),
        switchMap((action) => action.new ?
            this.__submit(action.value, action.responsible, null, null, action.customError) :
            this.__submit(action.value, action.responsible, action.statementId, action.taskId, action.customError))
    ));

    public constructor(
        private readonly actions: Actions,
        private readonly router: Router,
        private readonly submitAttachmentsEffect: SubmitAttachmentsEffect,
        private readonly taskEffect: ProcessTaskEffect,
        private readonly processApiService: ProcessApiService,
        private readonly statementsApiService: StatementsApiService,
    ) {

    }

    public __submit(
        value: IStatementInformationFormValue,
        responsible?: boolean,
        statementId?: number,
        taskId?: string,
        customError?: EErrorCode
    ): Observable<Action> {
        let isResponsible = responsible;
        return concat(
            statementId != null ?
                this.updateStatement(statementId, taskId, value) :
                this.createStatement(value, (_statementId) => statementId = _statementId).pipe(
                    endWithObservable(() => this.claimNext(statementId, (_taskId) => taskId = _taskId))
                ),
            // In case of a new statement, the defer call is required here
            // because statementId and taskId are created in the first step:
            defer(() => this.submitAttachmentsEffect.submit(statementId, taskId, value.attachments))
        ).pipe(
            catchErrorTo(setErrorAction({
                error: customError != null ? customError : EErrorCode.UNEXPECTED,
                statementId,
                setBothErrors: customError != null
            })),
            throwAfterActionType(setErrorAction),
            tap({error: () => isResponsible = undefined}),
            ignoreError(),
            endWithObservable(() => this.finalizeSubmit(statementId, taskId, isResponsible)),
            startWith(setStatementLoadingAction({loading: {submittingStatementInformation: true}})),
            endWith(setStatementLoadingAction({loading: {submittingStatementInformation: false}}))
        );
    }

    public createStatement(value: IStatementInformationFormValue, afterCreation: (statementId: number) => void): Observable<Action> {
        return this.statementsApiService.putStatement({
            title: value.title,
            dueDate: value.dueDate,
            departmentsDueDate: value.departmentsDueDate,
            receiptDate: value.receiptDate,
            typeId: value.typeId,
            city: value.city,
            district: value.district,
            contactId: value.contactId,
            sourceMailId: value.sourceMailId,
            creationDate: value.creationDate,
            customerReference: shrinkString(value.customerReference, null)
        }).pipe(
            map((info) => {
                const statementId = info.id;
                afterCreation(statementId);
                return updateStatementEntityAction({statementId, entity: {info}});
            }),
            catchPostStatementInfo("new")
        );
    }

    public updateStatement(statementId: number, taskId: string, value: IStatementInformationFormValue): Observable<Action> {
        return this.statementsApiService.postStatement(statementId, taskId, {
            title: value.title,
            dueDate: value.dueDate,
            departmentsDueDate: value.departmentsDueDate,
            receiptDate: value.receiptDate,
            typeId: value.typeId,
            city: value.city,
            district: value.district,
            contactId: value.contactId,
            sourceMailId: value.sourceMailId,
            creationDate: value.creationDate,
            customerReference: shrinkString(value.customerReference, null)
        }).pipe(
            map((info) => updateStatementEntityAction({statementId, entity: {info}})),
            catchPostStatementInfo(statementId)
        );
    }

    public claimNext(statementId: number, afterClaim: (taskId: string) => void): Observable<Action> {
        return this.taskEffect.claimNext(statementId, EAPIProcessTaskDefinitionKey.ADD_BASIC_INFO_DATA).pipe(
            filter((task) => task != null),
            map((task) => {
                afterClaim(task?.task?.taskId);
                return setTaskEntityAction({task: task?.task});
            })
        );
    }

    public finalizeSubmit(statementId: number, taskId: string, responsible?: boolean): Observable<Action> {
        if (statementId == null) {
            return EMPTY;
        }
        if (taskId != null && responsible != null) {
            return this.taskEffect.completeTask(statementId, taskId, {responsible: {type: "Boolean", value: responsible}}, true);
        } else {
            return this.taskEffect.navigateTo(statementId, taskId);
        }
    }

}

export function catchPostStatementInfo<T>(statementId: "new" | number): OperatorFunction<T, T | Action> {
    return pipe(
        catchHttpErrorTo(setErrorAction({
            statementId,
            error: EErrorCode.MISSING_FORM_DATA
        }), EHttpStatusCodes.BAD_REQUEST),
        catchHttpErrorTo(setErrorAction({
            statementId,
            error: EErrorCode.FAILED_LOADING_CONTACT
        }), EHttpStatusCodes.UNPROCESSABLE_ENTITY)
    );
}
