/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {NgModule} from "@angular/core";
import {EffectsModule} from "@ngrx/effects";
import {StoreModule} from "@ngrx/store";
import {DeleteEmailFromInboxEffect, DownloadEmailAttachmentEffect, FetchEmailsEffect} from "./effects";
import {EMAIL_NAME, EMAIL_REDCUERS} from "./mail-reducers.token";

@NgModule({
    imports: [
        StoreModule.forFeature(EMAIL_NAME, EMAIL_REDCUERS),
        EffectsModule.forFeature([
            DeleteEmailFromInboxEffect,
            DownloadEmailAttachmentEffect,
            FetchEmailsEffect
        ])
    ]
})
export class MailStoreModule {

}
