/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Injectable} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {Action} from "@ngrx/store";
import {concat, EMPTY, from, Observable} from "rxjs";
import {endWith, filter, ignoreElements, map, mergeMap, startWith} from "rxjs/operators";
import {MailApiService} from "../../../../core";
import {catchErrorTo} from "../../../../util/rxjs";
import {setErrorAction} from "../../../root/actions";
import {EErrorCode} from "../../../root/model";
import {deleteEmailEntityAction, deleteEmailFromInboxAction, setEmailLoadingStateAction} from "../../actions";

@Injectable({providedIn: "root"})
export class DeleteEmailFromInboxEffect {

    public delete$ = createEffect(() => this.actions.pipe(
        ofType(deleteEmailFromInboxAction),
        filter((action) => action.mailId != null),
        mergeMap((action) => this.delete(action.mailId))
    ));

    public constructor(
        public readonly actions: Actions,
        public readonly mailApiService: MailApiService,
        public readonly activatedRoute: ActivatedRoute,
        public readonly router: Router
    ) {

    }

    public delete(mailId: string): Observable<Action> {
        return concat(
            this.deleteEmailFromInbox(mailId),
            this.removeQueryParam(mailId)
        ).pipe(
            catchErrorTo(setErrorAction({error: EErrorCode.UNEXPECTED})),
            startWith(setEmailLoadingStateAction({loading: {deleting: true}})),
            endWith((setEmailLoadingStateAction({loading: {deleting: false}})))
        );
    }

    public deleteEmailFromInbox(mailId: string): Observable<Action> {
        return this.mailApiService.deleteInboxEmail(mailId).pipe(
            map(() => deleteEmailEntityAction({mailId}))
        );
    }

    public removeQueryParam(mailId: string): Observable<never> {
        if (this.activatedRoute.snapshot.queryParams.mailId !== mailId) {
            return EMPTY;
        }
        const navigatePromise = this.router.navigate([], {
            queryParams: {mailId: null},
            queryParamsHandling: "merge",
            replaceUrl: true
        });
        return from(navigatePromise).pipe(ignoreElements());
    }

}
