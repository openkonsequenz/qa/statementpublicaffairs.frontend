/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Inject, Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {Action} from "@ngrx/store";
import {EMPTY, Observable} from "rxjs";
import {filter, mergeMap} from "rxjs/operators";
import {AuthService, DownloadService, SPA_BACKEND_ROUTE} from "../../../../core";
import {urlJoin} from "../../../../util/http";
import {downloadEmailAttachmentAction} from "../../actions";

@Injectable({providedIn: "root"})
export class DownloadEmailAttachmentEffect {

    public download$ = createEffect(() => this.actions.pipe(
        ofType(downloadEmailAttachmentAction),
        filter((action) => action.mailId != null && action.name != null),
        mergeMap((action) => this.download(action.mailId, action.name))
    ));

    public constructor(
        public readonly actions: Actions,
        public readonly downloadService: DownloadService,
        public readonly authService: AuthService,
        @Inject(SPA_BACKEND_ROUTE) public readonly spaBackendRoute: string
    ) {

    }

    public download(mailId: string, name: string): Observable<Action> {
        name = encodeURI(name);
        const endPoint = `/mail/identifier/${mailId}/${name}`;
        this.downloadService.startDownload(urlJoin(this.spaBackendRoute, endPoint), this.authService.token);
        return EMPTY;
    }

}
