/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Action} from "@ngrx/store";
import {IAPIEmailModel} from "../../../../core/api/mail";
import {createEmailModelMock} from "../../../../test";
import {TStoreEntities} from "../../../../util/store";
import {deleteEmailEntityAction, setEmailEntitiesAction, setEmailInboxAction} from "../../actions";
import {mailEntitiesReducer} from "./mail-entities.reducer";

describe("mailEntitiesReducer", () => {
    const entities: IAPIEmailModel[] = [
        createEmailModelMock("<Mail19>"),
        createEmailModelMock("<Mail1919>")
    ];
    let initialState: TStoreEntities<IAPIEmailModel> = {};
    let state: TStoreEntities<IAPIEmailModel> = {};
    let action: Action;

    it("should update store entities on setEmailEntitiesAction", () => {
        initialState = {};
        action = setEmailEntitiesAction({entities: [null]});
        state = mailEntitiesReducer(initialState, action);
        expect(state).toEqual(initialState);

        action = setEmailEntitiesAction({entities});
        state = mailEntitiesReducer(initialState, action);
        expect(state).toEqual({
            [entities[0].identifier]: entities[0],
            [entities[1].identifier]: entities[1],
        });
    });

    it("should update store entities on setEmailInboxAction", () => {
        initialState = {};
        action = setEmailInboxAction({entities: [null]});
        state = mailEntitiesReducer(initialState, action);
        expect(state).toEqual(initialState);

        action = setEmailInboxAction({entities});
        state = mailEntitiesReducer(initialState, action);
        expect(state).toEqual({
            [entities[0].identifier]: entities[0],
            [entities[1].identifier]: entities[1],
        });
    });

    it("should delete store entities on deleteEmailEntityAction", () => {
        initialState = {
            [entities[0].identifier]: entities[0],
            [entities[1].identifier]: entities[1],
        };

        action = deleteEmailEntityAction({mailId: null});
        state = mailEntitiesReducer(initialState, action);
        expect(state).toEqual(initialState);

        action = deleteEmailEntityAction({mailId: entities[0].identifier});
        state = mailEntitiesReducer(initialState, action);
        expect(state).toEqual({
            [entities[1].identifier]: entities[1],
        });
    });

});
