/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Action} from "@ngrx/store";
import {IAPIEmailModel} from "../../../../core";
import {createEmailModelMock} from "../../../../test";
import {deleteEmailEntityAction, setEmailInboxAction} from "../../actions";
import {mailInboxReducer} from "./mail-inbox.reducer";

describe("mailInboxReducer", () => {

    let initialState: string[] = [];
    let state: string[] = [];
    let action: Action;

    it("should update inbox ids on setEmailInboxAction", () => {
        initialState = [];
        const entities: IAPIEmailModel[] = [
            createEmailModelMock("<Mail1>"),
            createEmailModelMock("<Mail2>")
        ];

        action = setEmailInboxAction({entities: [null]});
        state = mailInboxReducer(initialState, action);
        expect(state).toEqual([]);

        action = setEmailInboxAction({entities});
        state = mailInboxReducer(initialState, action);
        expect(state).toEqual([entities[0].identifier, entities[1].identifier]);
    });

    it("should delete inbox ids on deleteEmailEntityAction", () => {
        const entities: IAPIEmailModel[] = [
            createEmailModelMock("<Mail1>"),
            createEmailModelMock("<Mail2>")
        ];
        initialState = entities.map((_) => _.identifier);

        action = deleteEmailEntityAction({mailId: null});
        state = mailInboxReducer(initialState, action);
        expect(state).toEqual(initialState);

        action = deleteEmailEntityAction({mailId: entities[0].identifier});
        state = mailInboxReducer(initialState, action);
        expect(state).toEqual([entities[1].identifier]);
    });

});
