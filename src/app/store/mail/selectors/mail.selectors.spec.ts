/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPIEmailModel} from "../../../core/api/mail";
import {createEmailModelMock} from "../../../test";
import {TStoreEntities} from "../../../util/store";
import {getEmailInboxSelector} from "./mail.selectors";

describe("mailSelectors", () => {

    it("getEmailInboxSelector", () => {
        const projector = getEmailInboxSelector.projector;
        const inbox: IAPIEmailModel[] = [
            createEmailModelMock("<Mail1>"),
            createEmailModelMock("<Mail2>")
        ];
        const entities: TStoreEntities<IAPIEmailModel> = {
            [inbox[0].identifier]: inbox[0],
            [inbox[1].identifier]: inbox[1]
        };
        const inboxIds: string[] = inbox.map((_) => _.identifier);

        expect(projector({}, inboxIds)).toEqual([]);
        expect(projector(entities, [])).toEqual([]);
        expect(projector(entities, inboxIds)).toEqual(inbox.reverse());
    });

});
