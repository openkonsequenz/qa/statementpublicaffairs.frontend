/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {Action} from "@ngrx/store";
import {concat, EMPTY, merge, Observable, of} from "rxjs";
import {catchError, endWith, ignoreElements, mergeMap, startWith, switchMap} from "rxjs/operators";
import {SettingsApiService} from "../../../../core";
import {endWithObservable, ignoreError} from "../../../../util";
import {setErrorAction} from "../../../root/actions";
import {EErrorCode} from "../../../root/model";
import {fetchSettingsAction, setSettingsLoadingStateAction} from "../../../settings/actions";
import {submitTypesAction} from "../../../statements/actions";

@Injectable({providedIn: "root"})
export class SubmitTypesEffect {

    public submit$ = createEffect(() => this.actions.pipe(
        ofType(submitTypesAction),
        switchMap((action) => this.submit(action.types).pipe(
            ignoreError()
        ))
    ));

    public constructor(
        public readonly actions: Actions,
        public readonly settingsApiService: SettingsApiService
    ) {

    }

    public submit(
        types: { label: string; add?: boolean; delete?: boolean }[]
    ): Observable<Action> {
        const errors: string[] = [];

        const labelsToAdd = types.filter((_) => _.add).map((_) => _.label);
        const labelsToDelete = types.filter((_) => _.delete).map((_) => _.label);

        return merge(
            this.addTypes(labelsToAdd, errors),
            this.deleteTypes(labelsToDelete, errors)
        ).pipe(
            endWithObservable(() => errors.length > 0 ? concat(
                of(setErrorAction({error: EErrorCode.COULD_NOT_EDIT_TYPE, errorValue: {value: errors.toString().replace(/,/g, ", ")}})),
                of(fetchSettingsAction())
            ) : of(fetchSettingsAction())),
            startWith(setSettingsLoadingStateAction({state: {submittingTypes: true}})),
            endWith(setSettingsLoadingStateAction({state: {submittingTypes: false}}))
        );
    }

    public addTypes(labels: string[], errors: any[]): Observable<Action> {
        return of(...labels).pipe(
            mergeMap((item) => this.settingsApiService.addNewType(item).pipe(
                catchError(() => {
                    errors.push(item);
                    return EMPTY;
                }),
                ignoreElements()
            ))
        );
    }

    public deleteTypes(labels: string[], errors: any[]): Observable<Action> {
        return of(...labels).pipe(
            mergeMap((item) => this.settingsApiService.deleteType(item).pipe(
                catchError(() => {
                    errors.push(item);
                    return EMPTY;
                }),
                ignoreElements()
            ))
        );
    }

}
