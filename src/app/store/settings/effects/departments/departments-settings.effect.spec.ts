/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {TestBed} from "@angular/core/testing";
import {provideMockActions} from "@ngrx/effects/testing";
import {Action} from "@ngrx/store";
import {Observable, Subject, Subscription} from "rxjs";
import {IAPIDepartmentTable, SPA_BACKEND_ROUTE} from "../../../../core";
import {
    fetchDepartmentsSettingsAction,
    setDepartmentsSettingsAction,
    setSettingsLoadingStateAction,
    submitDepartmentsSettingsAction
} from "../../actions";
import {DepartmentsSettingsEffect} from "./departments-settings.effect";

describe("DepartmentsSettingsEffect", () => {

    let actions$: Observable<Action>;
    let httpTestingController: HttpTestingController;
    let effect: DepartmentsSettingsEffect;
    let subscription: Subscription;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                provideMockActions(() => actions$),
                {
                    provide: SPA_BACKEND_ROUTE,
                    useValue: "/"
                }
            ]
        });
        effect = TestBed.inject(DepartmentsSettingsEffect);
        httpTestingController = TestBed.inject(HttpTestingController);
    });

    afterEach(() => {
        if (subscription != null) {
            subscription.unsubscribe();
        }
    });

    it("should fetch department settings", () => {
        const results: Action[] = [];
        const actionSubject = new Subject<Action>();
        const data: IAPIDepartmentTable = {};
        actions$ = actionSubject;
        subscription = effect.fetch$.subscribe((_) => results.push(_));

        actionSubject.next(fetchDepartmentsSettingsAction());
        expectGetDepartmentsSettingsRequest(data);

        expect(results).toEqual([
            setSettingsLoadingStateAction({state: {fetchingDepartments: true}}),
            setDepartmentsSettingsAction({data}),
            setSettingsLoadingStateAction({state: {fetchingDepartments: false}})
        ]);
        httpTestingController.verify();
    });

    it("should submit departments settings", () => {
        const results: Action[] = [];
        const actionSubject = new Subject<Action>();
        const data: IAPIDepartmentTable = {};
        actions$ = actionSubject;
        subscription = effect.submit$.subscribe((_) => results.push(_));

        actionSubject.next(submitDepartmentsSettingsAction({data}));
        expectPutDepartmentsSettingsRequest(data);
        expectGetDepartmentsSettingsRequest(data);

        expect(results).toEqual([
            setSettingsLoadingStateAction({state: {submittingDepartments: true}}),
            setSettingsLoadingStateAction({state: {fetchingDepartments: true}}),
            setDepartmentsSettingsAction({data}),
            setSettingsLoadingStateAction({state: {fetchingDepartments: false}}),
            setSettingsLoadingStateAction({state: {submittingDepartments: false}})
        ]);
        httpTestingController.verify();
    });

    function expectGetDepartmentsSettingsRequest(data: IAPIDepartmentTable) {
        const url = "/admin/departments";
        const request = httpTestingController.expectOne(url);
        expect(request.request.method).toBe("GET");
        request.flush(data);
    }

    function expectPutDepartmentsSettingsRequest(data: IAPIDepartmentTable) {
        const url = "/admin/departments";
        const request = httpTestingController.expectOne(url);
        expect(request.request.method).toBe("PUT");
        expect(request.request.body).toEqual(data);
        request.flush(data);
    }

});

