/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {InjectionToken} from "@angular/core";
import {ActionReducerMap} from "@ngrx/store";
import {ISettingsStoreState} from "./model";
import {
    departmentsSettingsReducer,
    sectorsReducer,
    settingsLoadingReducer,
    statementTypesReducer,
    textblockSettingsReducer
} from "./reducers";
import {usersSettingsReducer} from "./reducers/users/users.reducer";

export const SETTINGS_FEATURE_NAME = "settings";

export const SETTINGS_REDUCER = new InjectionToken<ActionReducerMap<ISettingsStoreState>>("Settings store reducer", {
    providedIn: "root",
    factory: () => ({
        departments: departmentsSettingsReducer,
        loading: settingsLoadingReducer,
        sectors: sectorsReducer,
        statementTypes: statementTypesReducer,
        users: usersSettingsReducer,
        textblock: textblockSettingsReducer
    })
});
