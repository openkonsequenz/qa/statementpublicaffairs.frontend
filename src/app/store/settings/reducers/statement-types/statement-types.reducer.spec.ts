/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPIStatementType} from "../../../../core/api/settings";
import {TStoreEntities} from "../../../../util/store";
import {setStatementTypesAction} from "../../actions";
import {statementTypesReducer} from "./statement-types.reducer";

describe("statementTypesReducer", () => {

    it("should not change the state on setStatementTypesAction with no values given", () => {
        const initialState: TStoreEntities<IAPIStatementType> = {};
        let action = setStatementTypesAction(undefined);
        let state = statementTypesReducer(initialState, action);
        expect(state).toEqual(initialState);

        action = setStatementTypesAction({statementTypes: null});
        state = statementTypesReducer(initialState, action);
        expect(state).toEqual(initialState);

        action = setStatementTypesAction({statementTypes: []});
        state = statementTypesReducer(initialState, action);
        expect(state).toEqual(initialState);
    });

    it("should add the statementtypes to the state object", () => {
        const initialState: TStoreEntities<IAPIStatementType> = {};
        const action = setStatementTypesAction({
            statementTypes: [
                {
                    id: 1,
                    name: "name"
                },
                {
                    id: 2,
                    name: "name2"
                }
            ]
        });
        const state = statementTypesReducer(initialState, action);
        expect(state).toEqual({
            1: {
                id: 1,
                name: "name"
            },
            2: {
                id: 2,
                name: "name2"
            }
        });
    });

});
