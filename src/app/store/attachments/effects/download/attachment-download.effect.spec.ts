/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {TestBed} from "@angular/core/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {provideMockActions} from "@ngrx/effects/testing";
import {Action} from "@ngrx/store";
import {Observable, of, Subscription} from "rxjs";
import {SPA_BACKEND_ROUTE} from "../../../../core/external-routes";
import {startAttachmentDownloadAction} from "../../actions";
import {AttachmentDownloadEffect} from "./attachment-download.effect";


describe("AttachmentDownloadEffect", () => {

    const token = "" + Math.floor(10000 * Math.random());

    let actions$: Observable<Action>;
    let effect: AttachmentDownloadEffect;
    let subscription: Subscription;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule
            ],
            providers: [
                provideMockActions(() => actions$),
                {
                    provide: SPA_BACKEND_ROUTE,
                    useValue: "/statementpaBE"
                }
            ]
        });
        effect = TestBed.inject(AttachmentDownloadEffect);
    });

    afterEach(() => {
        if (subscription != null) {
            subscription.unsubscribe();
        }
    });

    it("should start downloads of attachments", async () => {
        const statementId = 19;
        const attachmentId = 1919;
        const downloadSpy = spyOn(effect.downloadService, "startDownload");
        effect.authService = {token} as any;

        actions$ = of(startAttachmentDownloadAction({statementId, attachmentId}));
        subscription = effect.startAttachmentDownload$.subscribe();
        await Promise.resolve();
        expect(downloadSpy).toHaveBeenCalledWith(`/statementpaBE/statements/${statementId}/attachments/${attachmentId}/file`, token);
    });

});
