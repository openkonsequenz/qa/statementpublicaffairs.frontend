/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {FormArray, FormControl} from "@angular/forms";
import {createFormGroup} from "../../../util/forms";
import {IAttachmentControlValue} from "./IAttachmentControlValue";

export interface IAttachmentFormValue {

    add?: IAttachmentControlValue[];

    edit?: IAttachmentControlValue[];

    email?: IAttachmentControlValue[];

    transferMailText?: boolean;

    mailTextAttachmentId?: number;

}

export function createAttachmentForm() {
    return createFormGroup<IAttachmentFormValue>({
        add: new FormArray([]),
        edit: new FormArray([]),
        email: new FormArray([]),
        transferMailText: new FormControl(false),
        mailTextAttachmentId: new FormControl(undefined)
    });
}
