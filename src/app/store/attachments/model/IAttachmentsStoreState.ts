/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPIAttachmentModel, IAPIAttachmentTag} from "../../../core/api";
import {TStoreEntities} from "../../../util/store";
import {IAttachmentControlValue} from "./IAttachmentControlValue";

export interface IAttachmentsStoreState {

    entities: TStoreEntities<IAPIAttachmentModel>;

    statementAttachments: TStoreEntities<number[]>;

    statementCache?: TStoreEntities<IAttachmentControlValue[]>;

    tags?: IAPIAttachmentTag[];

}
