/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createAttachmentModelMock} from "../../../../test";
import {addAttachmentEntityAction, deleteAttachmentsAction, setAttachmentsAction} from "../../actions";
import {IAttachmentsStoreState} from "../../model";
import {statementAttachmentsReducer} from "./statement-attachments.reducer";

describe("statementAttachmentsReducer", () => {

    const initialState: IAttachmentsStoreState["statementAttachments"] = {
        1919: [1, 2, 3]
    };

    it("should set a list of attachment entity IDs to store", () => {
        const attachment = createAttachmentModelMock(19, "1", "9");
        const action = setAttachmentsAction({
            statementId: 1919,
            entities: [attachment, null]
        });
        const state = statementAttachmentsReducer(initialState, action);
        expect(state).toEqual({
            ...initialState,
            1919: [attachment.id]
        });
    });

    it("should add a single attachment ID to store", () => {
        const attachment = createAttachmentModelMock(19, "1", "9");
        const action = addAttachmentEntityAction({
            statementId: 1919,
            entity: null
        });
        let state = statementAttachmentsReducer(initialState, action);
        expect(state).toEqual(initialState);
        action.entity = attachment;
        state = statementAttachmentsReducer(initialState, action);
        expect(state).toEqual({
            ...initialState,
            1919: [1, 2, 3, attachment.id]
        });
    });

    it("should delete attachment IDs from store", () => {
        const action = deleteAttachmentsAction({
            statementId: 1919,
            entityIds: [2]
        });
        const state = statementAttachmentsReducer(initialState, action);
        expect(state).toEqual({
            1919: [1, 3]
        });
    });

});
