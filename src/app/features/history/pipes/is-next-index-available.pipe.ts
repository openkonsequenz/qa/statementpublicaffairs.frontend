/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Pipe, PipeTransform} from "@angular/core";
import {ISelectOption} from "../../../shared/controls/select";

@Pipe({
    name: "isNextIndexAvailable"
})
export class IsNextIndexAvailablePipe implements PipeTransform {

    public transform(versions: ISelectOption<string>[], selectedOption: string, indexIncrement: number) {
        if (selectedOption == null || versions == null) {
            return false;
        }
        const currentVersion = versions.find((version) => version.value === selectedOption);
        const currentVersionIndex = versions.indexOf(currentVersion);
        const newIndex = currentVersionIndex + indexIncrement;
        return newIndex < versions.length && newIndex >= 0;
    }

}
