/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {ISelectOption} from "../../../shared/controls/select";
import {IsNextIndexAvailablePipe} from "./is-next-index-available.pipe";

describe("IsNextIndexAvailablePipe", () => {

    const pipe = new IsNextIndexAvailablePipe();

    it("should return if the index after incrementing is in range", () => {
        const versions: ISelectOption<string>[] = [
            {label: "label1", value: "value1"},
            {label: "label2", value: "value2"},
            {label: "label3", value: "value3"}
        ];

        let result = pipe.transform(versions, "value1", 1);
        expect(result).toBeTrue();

        result = pipe.transform(versions, "value3", 1);
        expect(result).toBeFalse();

        result = pipe.transform(versions, "value3", -1);
        expect(result).toBeTrue();

        result = pipe.transform(versions, "value1", -1);
        expect(result).toBeFalse();
    });
});


