/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {ISelectOption} from "../../../shared/controls/select";
import {IsDetailsModeDetectedPipe} from "./is-details-mode-selected.pipe";

describe("IsDetailsModeDetectedPipe", () => {

    const pipe = new IsDetailsModeDetectedPipe();

    it("should return whether the details option is selected", () => {
        const normalOption: ISelectOption<string> = {
            label: "normal",
            value: "normal"
        };
        const detailsOption: ISelectOption<string> = {
            label: "details",
            value: "details"
        };
        let result = pipe.transform(normalOption);
        expect(result).toBeFalse();

        result = pipe.transform(detailsOption);
        expect(result).toBeTrue();

        result = pipe.transform(null);
        expect(result).toBeFalse();

        result = pipe.transform({
            label: "label",
            value: "value"
        });
        expect(result).toBeFalse();
    });
});


