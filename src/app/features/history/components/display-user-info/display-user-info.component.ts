/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, forwardRef, Input} from "@angular/core";
import {NG_VALUE_ACCESSOR} from "@angular/forms";
import {momentFormatDisplayFullDateAndTime} from "../../../../util";

@Component({
    selector: "app-display-user-info",
    templateUrl: "./display-user-info.component.html",
    styleUrls: ["./display-user-info.component.scss"],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => DisplayUserInfoComponent),
            multi: true
        }
    ]
})
export class DisplayUserInfoComponent {

    @Input()
    public appTextBlockModel: {
        version: string;

        timestamp: string;

        user: string;

        firstName: string;

        lastName: string;
    };

    public timeFormat = momentFormatDisplayFullDateAndTime;
}
