/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, EventEmitter, forwardRef, Input, Output} from "@angular/core";
import {NG_VALUE_ACCESSOR} from "@angular/forms";
import {IStoreTextblockHistoryVersionModel} from "../../../../core/api/statements/IAPITextblockHistoryVersionModel";
import {ISelectOption} from "../../../../shared/controls/select";
import {momentFormatDisplayFullDateAndTime} from "../../../../util";

@Component({
    selector: "app-version-info",
    templateUrl: "./version-info.component.html",
    styleUrls: ["./version-info.component.scss"],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => VersionInfoComponent),
            multi: true
        }
    ]
})
export class VersionInfoComponent {

    @Input()
    public appSelectVersion: boolean;

    @Input()
    public appSelectedVersion: IStoreTextblockHistoryVersionModel;

    @Input()
    public appSelectedOption: string;

    @Input()
    public appVersionOptions: ISelectOption<string>[];

    @Input()
    public appModeOptions: ISelectOption<string>[];

    @Input()
    public appEnableDetailsMode: boolean;

    @Input()
    public selectedMode: ISelectOption<string>;

    @Input()
    public appDisabled: boolean;

    @Output()
    public appVersionSelect = new EventEmitter<string>();

    @Output()
    public appNextVersion = new EventEmitter<number>();

    @Output()
    public appModeChange = new EventEmitter();

    public timeFormat = momentFormatDisplayFullDateAndTime;
}
