/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {withKnobs} from "@storybook/addon-knobs";
import {moduleMetadata, storiesOf} from "@storybook/angular";
import {I18nModule} from "../../../../core";
import {SearchModule} from "../../search.module";
import {SearchFilterComponent} from "./search-filter.component";

storiesOf("Features / search", module)
    .addDecorator(withKnobs)
    .addDecorator(moduleMetadata({
        declarations: [],
        imports: [
            I18nModule,
            SearchModule
        ]
    }))
    .add("SearchFilterComponent", () => ({
        component: SearchFilterComponent,
        props: {}
    }));
