/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createSelector} from "@ngrx/store";
import {EAPIProcessTaskDefinitionKey} from "../../../core";
import {taskSelector} from "../../../store";
import {EStatementEditSites} from "../model";

export const statementEditSiteSelector = createSelector(
    taskSelector,
    (task): EStatementEditSites => {
        if (task == null) {
            return EStatementEditSites.LOADING;
        }

        switch (task.taskDefinitionKey) {
        case EAPIProcessTaskDefinitionKey.ADD_BASIC_INFO_DATA:
            return EStatementEditSites.STATEMENT_INFORMATION_FORM;

        case EAPIProcessTaskDefinitionKey.ADD_WORK_FLOW_DATA:
            return EStatementEditSites.WORKFLOW_DATA_FORM;

        case EAPIProcessTaskDefinitionKey.CREATE_NEGATIVE_RESPONSE:
        case EAPIProcessTaskDefinitionKey.CREATE_DRAFT:
        case EAPIProcessTaskDefinitionKey.ENRICH_DRAFT:
        case EAPIProcessTaskDefinitionKey.CHECK_AND_FORMULATE_RESPONSE:
            return EStatementEditSites.STATEMENT_EDITOR_FORM;
        }

    }
);
