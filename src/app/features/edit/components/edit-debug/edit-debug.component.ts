/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from "@angular/core";
import {IAPIProcessObject, IAPIProcessTask} from "../../../../core";

@Component({
    selector: "app-edit-debug",
    templateUrl: "./edit-debug.component.html",
    styleUrls: ["./edit-debug.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditDebugComponent {

    @Input()
    public appSite: string;

    @Input()
    public appTask: IAPIProcessTask;

    @Output()
    public appSubmit: EventEmitter<IAPIProcessObject> = new EventEmitter<IAPIProcessObject>();

    public toString(arg: any): string {
        if (typeof arg === "object") {
            return JSON.stringify(arg);
        }
        return "" + arg;
    }

    public submit(value: boolean) {
        const obj: IAPIProcessObject = {};
        Object.keys(this.appTask.requiredVariables)
            .filter((name) => this.appTask.requiredVariables[name] === "Boolean")
            .forEach((name) => obj[name] = {type: "Boolean", value});
        this.appSubmit.emit(obj);
    }

}
