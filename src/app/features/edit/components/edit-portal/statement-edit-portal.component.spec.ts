/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {CommonModule} from "@angular/common";
import {ComponentFixture, TestBed, waitForAsync} from "@angular/core/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {MockStore, provideMockStore} from "@ngrx/store/testing";
import {I18nModule} from "../../../../core/i18n";
import {SharedPipesModule} from "../../../../shared/pipes";
import {ProgressSpinnerModule} from "../../../../shared/progress-spinner";
import {completeTaskAction, fetchStatementDetailsAction, queryParamsIdSelector, taskSelector} from "../../../../store";
import {CommentsFormModule} from "../../../forms/comments";
import {StatementInformationFormModule} from "../../../forms/statement-information";
import {WorkflowDataFormModule} from "../../../forms/workflow-data/workflow-data-form.module";
import {StatementEditRoutingModule} from "../../statement-edit-routing.module";
import {StatementEditPortalComponent} from "./statement-edit-portal.component";

describe("StatementEditPortalComponent", () => {
    let component: StatementEditPortalComponent;
    let fixture: ComponentFixture<StatementEditPortalComponent>;
    let mockStore: MockStore;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [
                CommonModule,
                I18nModule,
                StatementEditRoutingModule,
                ProgressSpinnerModule,
                CommentsFormModule,
                StatementInformationFormModule,
                WorkflowDataFormModule,
                SharedPipesModule,

                RouterTestingModule
            ],
            providers: [
                provideMockStore({initialState: {process: {}}})
            ],
            declarations: [
                StatementEditPortalComponent
            ]
        }).compileComponents();

        mockStore = TestBed.inject(MockStore);
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(StatementEditPortalComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });


    it("should dispatch fetch details action", () => {
        const queryParamsIdSelectorMock = mockStore.overrideSelector(queryParamsIdSelector, undefined);
        const dispatchSpy = spyOn(mockStore, "dispatch");
        const action = fetchStatementDetailsAction({statementId: 19});
        queryParamsIdSelectorMock.setResult(19);
        mockStore.refreshState();
        component.ngOnInit();
        expect(dispatchSpy).toHaveBeenCalledWith(action);
        component.ngOnDestroy();
    });

    it("should dispatch complete task action", async () => {
        const taskSelectorMock = mockStore.overrideSelector(taskSelector, undefined);
        const dispatchSpy = spyOn(mockStore, "dispatch");
        const action = completeTaskAction({
            statementId: 1,
            taskId: "19",
            variables: {responsible: {type: "Boolean", value: true}},
            claimNext: true
        });
        taskSelectorMock.setResult({statementId: 1, taskId: "19"} as any);
        await component.completeTask(action.variables);
        expect(dispatchSpy).toHaveBeenCalledWith(action);
        component.ngOnDestroy();
    });

});
