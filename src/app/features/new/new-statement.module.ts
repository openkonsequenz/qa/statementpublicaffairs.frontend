/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {NgModule} from "@angular/core";
import {StatementInformationFormModule} from "../forms/statement-information";
import {NewStatementComponent} from "./components";

@NgModule({
    imports: [
        StatementInformationFormModule
    ],
    declarations: [
        NewStatementComponent
    ],
    exports: [
        NewStatementComponent
    ]
})
export class NewStatementModule {

}
