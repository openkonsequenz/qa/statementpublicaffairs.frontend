/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Directive, Inject, Input, NgZone} from "@angular/core";
import {LatLngLiteral} from "leaflet";
import {ILeafletConfiguration, LEAFLET_CONFIGURATION_TOKEN} from "../../leaflet-configuration.token";
import {stringToLatLngZoom} from "../../util";
import {LeafletHandler} from "../leaflet";
import {AbstractLeafletMarkerDirective} from "./abstract-leaflet-marker.directive";

@Directive({
    selector: "[appLeafletMarker]",
    exportAs: "appLeafletMarker"
})
export class LeafletMarkerDirective extends AbstractLeafletMarkerDirective {

    public constructor(
        ngZone: NgZone,
        leafletHandler: LeafletHandler,
        @Inject(LEAFLET_CONFIGURATION_TOKEN) configuration: ILeafletConfiguration,
    ) {
        super(ngZone, leafletHandler, configuration);
    }

    @Input()
    public set appLeafletMarker(value: LatLngLiteral | string) {
        const isSet = this.setLatLng(typeof value === "string" ? stringToLatLngZoom(value) : value);
        if (!isSet) {
            this.remove();
        }
    }

}
