/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, ViewChild} from "@angular/core";
import {ComponentFixture, TestBed, waitForAsync} from "@angular/core/testing";
import {LatLngLiteral} from "leaflet";
import {Subject, Subscription} from "rxjs";
import {TestCoreModule} from "../../../../core/test-core.module";
import {MapModule} from "../../map.module";
import {LeafletMarkerDirective} from "./leaflet-marker.directive";

describe("LeafletMarkerDirective", () => {

    const latLng: LatLngLiteral = {
        lat: 52.520008,
        lng: 13.404954
    };

    let component: LeafletMarkerSpecComponent;
    let fixture: ComponentFixture<LeafletMarkerSpecComponent>;
    let directive: LeafletMarkerDirective;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [
                MapModule,
                TestCoreModule
            ],
            declarations: [LeafletMarkerSpecComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LeafletMarkerSpecComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        directive = component.directive;
    });

    it("should set coordinates for marker", () => {
        const removeSpy = spyOn(directive.marker, "remove").and.callThrough();

        directive.appLeafletMarker = latLng;
        expect(directive.marker.getLatLng().lat).toEqual(latLng.lat);
        expect(directive.marker.getLatLng().lng).toEqual(latLng.lng);

        directive.appLeafletMarker = "1,2,12";
        expect(directive.marker.getLatLng().lat).toEqual(1);
        expect(directive.marker.getLatLng().lng).toEqual(2);

        directive.appLeafletMarker = {lat: 1, lng: null};
        expect(removeSpy).toHaveBeenCalled();
        directive.appLeafletMarker = {lat: null, lng: 2};
        expect(removeSpy).toHaveBeenCalled();
        directive.appLeafletMarker = null;
        expect(removeSpy).toHaveBeenCalled();
    });

    it("should pass through leaflet events as output", () => {
        const event$ = new Subject<any>();
        const onSpy = spyOn(directive, "on").and.returnValue(event$.asObservable());

        onSpy.calls.reset();
        const subscription = directive.appClick.subscribe();
        expect(onSpy).toHaveBeenCalledWith("click");
        subscription.unsubscribe();
    });

    it("should generate event observables for leaflet map", () => {
        let subscription: Subscription = null;
        expect(() => subscription = directive.on("click", true).subscribe()).not.toThrow();
        subscription.unsubscribe();
    });

});

@Component({
    selector: "app-leaflet-spec",
    template: `
        <div appLeaflet>
            <ng-container [appLeafletMarker]="coordinates">
            </ng-container>
        </div>
    `
})
class LeafletMarkerSpecComponent {

    public coordinates: LatLngLiteral;

    @ViewChild(LeafletMarkerDirective, {static: true})
    public directive: LeafletMarkerDirective;

}
