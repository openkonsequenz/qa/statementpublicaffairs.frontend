/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {InjectionToken} from "@angular/core";
import {Observable} from "rxjs";
import {IAppConfiguration} from "../../core";

export type ILeafletConfiguration = IAppConfiguration["leaflet"];

export const LEAFLET_RESIZE_TOKEN = new InjectionToken<Observable<any>>("Leaflet resize observable");

export const LEAFLET_CONFIGURATION_TOKEN = new InjectionToken<ILeafletConfiguration>("Leaflet configuration");
