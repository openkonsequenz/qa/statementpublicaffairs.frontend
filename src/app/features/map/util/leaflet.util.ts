/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Evented, LatLngLiteral, LeafletEvent} from "leaflet";
import {EMPTY, fromEventPattern, Observable} from "rxjs";
import {NodeEventHandler} from "rxjs/internal/observable/fromEvent";

/**
 * Creates an observable for specific leaflet map events.
 */
export function fromLeafletEvent<T extends LeafletEvent>(leafletMap: Evented, type: string): Observable<T> {
    if (leafletMap == null) {
        return EMPTY;
    }
    const addHandler = (handler: NodeEventHandler) => leafletMap.on(type, handler);
    const removeHandler = (handler: NodeEventHandler) => leafletMap.off(type, handler);
    return fromEventPattern<T>(addHandler, removeHandler);
}

/**
 * Reduces the given variables to a comma separated string.
 */
export function latLngZoomToString(latLng: LatLngLiteral, zoom: number): string {
    const values = [
        latLng?.lat,
        latLng?.lng,
        zoom
    ];

    return values.some((_) => !Number.isFinite(_)) ?
        undefined :
        values.reduce((_, v) => _ + "," + v, "").slice(1);
}

/**
 * Parses a string as comma separated list of coordination/zoom values.
 */
export function stringToLatLngZoom(value: string): LatLngLiteral & { zoom: number } {
    const split = typeof value !== "string" ? [] : value.split(",")
        .map((v) => parseFloat(v.replace(",", "")));

    return split.length !== 3 || split.some((_) => !Number.isFinite(_)) ? undefined : {
        lat: split[0],
        lng: split[1],
        zoom: split[2]
    };
}
