/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, OnDestroy, OnInit} from "@angular/core";
import {select, Store} from "@ngrx/store";
import {combineLatest, Observable, Subject} from "rxjs";
import {filter, take, takeUntil, withLatestFrom} from "rxjs/operators";
import {
    addCommentAction,
    currentActivityIds,
    deleteCommentAction,
    fetchStatementDetailsAction,
    getStatementErrorSelector,
    historySelector,
    IStatementEntity,
    IStatementErrorEntity,
    processDiagramSelector,
    processLoadingSelector,
    processNameSelector,
    processVersionSelector,
    queryParamsIdSelector,
    setErrorAction,
    statementCanceledSelector,
    statementLoadingSelector,
    statementSelector,
    statementTasksSelector,
    statementTitleSelector,
    userNameSelector,
    userRolesSelector
} from "../../../../store";


/**
 * This component displays all information saved for the selected statement.
 * Data cannot be edited. Only comments can be added to the statement.
 */

@Component({
    selector: "app-statement-details",
    templateUrl: "./statement-details.component.html",
    styleUrls: ["./statement-details.component.scss"]
})
export class StatementDetailsComponent implements OnInit, OnDestroy {

    public statementId$ = this.store.pipe(select(queryParamsIdSelector));

    public statement$: Observable<IStatementEntity> = this.store.pipe(select(statementSelector));

    public statementCanceled$: Observable<boolean> = this.store.pipe(select(statementCanceledSelector));

    public title$ = this.store.pipe(select(statementTitleSelector));

    public processName$ = this.store.pipe(select(processNameSelector));

    public processVersion$ = this.store.pipe(select(processVersionSelector));

    public processHistory$ = this.store.pipe(select(historySelector));

    public processDiagram$ = this.store.pipe(select(processDiagramSelector));

    public processCurrentActivityIds$ = this.store.pipe(select(currentActivityIds));

    public userRoles$ = this.store.pipe(select(userRolesSelector));

    public userName$ = this.store.pipe(select(userNameSelector));

    public tasks$ = this.store.pipe(select(statementTasksSelector));

    public isStatementLoading$ = this.store.pipe(select(statementLoadingSelector));

    public appError$: Observable<IStatementErrorEntity> = this.store.pipe(select(getStatementErrorSelector));

    public tasksLoading$ = this.store.pipe(select(processLoadingSelector));

    private destroy$ = new Subject<void>();

    public constructor(public readonly store: Store) {

    }

    public ngOnInit(): void {
        this.statementId$
            .pipe(
                filter((statementId) => statementId != null),
                takeUntil(this.destroy$)
            )
            .subscribe((statementId) => this.store.dispatch(fetchStatementDetailsAction({statementId})));

        this.tasksLoading$.pipe(
            filter((tasks) => tasks === false),
            withLatestFrom(this.statementId$),
            takeUntil(this.destroy$)
        ).subscribe(([loading, statementId]) => {
            this.store.dispatch(fetchStatementDetailsAction({statementId}));
        });
    }

    public async ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
        this.clearErrors();
    }

    public async addComment(text: string) {
        const statementId = await this.statementId$.pipe(take(1)).toPromise();
        this.store.dispatch(addCommentAction({statementId, text}));
    }

    public async deleteComment(commentId: number) {
        const statementId = await this.statementId$.pipe(take(1)).toPromise();
        this.store.dispatch(deleteCommentAction({statementId, commentId}));
    }

    public clearErrors() {
        combineLatest([this.statementId$, this.isStatementLoading$]).pipe(
            take(1),
            filter(([statementId, loading]) => statementId != null && !loading)
        ).subscribe(([statementId]) => this.store.dispatch(setErrorAction({statementId, error: null})));
    }

}
