/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {EventEmitter} from "@angular/core";
import {ComponentFixture, TestBed, waitForAsync} from "@angular/core/testing";
import {MockStore, provideMockStore} from "@ngrx/store/testing";
import {of, timer} from "rxjs";
import {
    APP_CONFIGURATION,
    I18nModule,
    IAPIProcessTask,
    IAPISearchOptions,
    IAPIStatementModel,
    IAPIStatementType
} from "../../../../../core";
import {IAPIClaimDetails} from "../../../../../core/api/process/IAPIClaimDetails";
import {
    cancelStatementAction,
    claimDetailsSelector,
    EErrorCode,
    fetchContactDetailsAction,
    fetchSettingsAction,
    getStatementLoadingSelector,
    IStatementInformationFormValue,
    openContactDataBaseAction,
    setErrorAction,
    startContactSearchAction,
    statementInformationFormValueSelector,
    statementInfoSelector,
    statementTypeOptionsSelector,
    statementTypesSelector,
    submitStatementInformationFormAction,
    taskSelector,
    userNameSelector
} from "../../../../../store";
import {createSelectOptionsMock} from "../../../../../test";
import {StatementInformationFormModule} from "../../statement-information-form.module";
import {StatementInformationFormComponent} from "./statement-information-form.component";

describe("StatementInformationFormComponent", () => {
    const today = new Date().toISOString().slice(0, 10);
    let component: StatementInformationFormComponent;
    let fixture: ComponentFixture<StatementInformationFormComponent>;
    const task: Partial<IAPIProcessTask> = {
        taskId: "19191919",
        statementId: 19
    };
    const types: IAPIStatementType[] = [{
        id: 0,
        name: "type0"
    }];
    let mockStore: MockStore;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [
                I18nModule,
                StatementInformationFormModule
            ],
            providers: [
                provideMockStore({
                    initialState: {statements: {}, settings: {}, contacts: {}, attachments: {}},
                    selectors: [
                        {
                            selector: statementTypeOptionsSelector,
                            value: createSelectOptionsMock(5, "Statement Type")
                        },
                        {
                            selector: taskSelector,
                            value: task
                        },
                        {
                            selector: statementTypesSelector,
                            value: types
                        }
                    ]
                }),
                {
                    provide: APP_CONFIGURATION, useValue: {
                        claimDetails: {
                            displayClaimDetails: false,
                            pollClaimDetails: false,
                        }
                    }
                }
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(StatementInformationFormComponent);
        mockStore = TestBed.inject(MockStore);
        component = fixture.componentInstance;
    });

    it("should initialize form for existing statements", async () => {
        const statementInformationFormValueSelectorMock = mockStore.overrideSelector(statementInformationFormValueSelector, {});
        const value: IStatementInformationFormValue = createStatementInfoFormValue({
            title: "Title",
            typeId: 19
        });
        expect(component).toBeDefined();
        fixture.detectChanges();
        await fixture.whenStable();
        statementInformationFormValueSelectorMock.setResult({...value});
        mockStore.refreshState();
        await fixture.whenStable();
        expect(component.appFormGroup.touched).toBeTrue();
        expect(component.getValue()).toEqual(value);
    });

    it("should initialize for new statements", async () => {
        const statementInformationFormValueSelectorMock = mockStore.overrideSelector(statementInformationFormValueSelector, {});
        const value = createStatementInfoFormValue({
            departmentsDueDate: today,
            dueDate: today,
            receiptDate: today,
            creationDate: today,
            typeId: 0
        });
        component.appForNewStatement = true;
        expect(component).toBeDefined();
        fixture.detectChanges();
        await fixture.whenStable();
        statementInformationFormValueSelectorMock.setResult({title: "Title"});
        mockStore.refreshState();
        await fixture.whenStable();
        expect(component.appFormGroup.untouched).toBeTrue();
        expect(component.getValue()).toEqual(value);
    });

    it("should initialize for new statements without statement types", async () => {
        const statementInformationFormValueSelectorMock = mockStore.overrideSelector(statementInformationFormValueSelector, {});
        mockStore.overrideSelector(statementTypeOptionsSelector, null);
        mockStore.overrideSelector(statementTypesSelector, null);
        const value = createStatementInfoFormValue({
            typeId: undefined,
            departmentsDueDate: today,
            dueDate: today,
            receiptDate: today,
            creationDate: today
        });
        component.appForNewStatement = true;
        expect(component).toBeDefined();
        fixture.detectChanges();
        await fixture.whenStable();
        statementInformationFormValueSelectorMock.setResult({title: "Title"});
        mockStore.refreshState();
        await fixture.whenStable();
        expect(component.appFormGroup.untouched).toBeTrue();
        expect(component.getValue()).toEqual(value);
    });

    it("should disable form when loading", async () => {
        const getStatementLoadingSelectorMock = mockStore.overrideSelector(getStatementLoadingSelector, {});
        fixture.detectChanges();
        await fixture.whenStable();

        getStatementLoadingSelectorMock.setResult({});
        mockStore.refreshState();

        expect(component.appFormGroup.enabled).toBeTrue();

        getStatementLoadingSelectorMock.setResult({submittingStatementInformation: true});
        component.appFormGroup.disable();
        mockStore.refreshState();

        expect(component.appFormGroup.enabled).toBeFalse();

        getStatementLoadingSelectorMock.setResult({submittingStatementInformation: false});
        component.appFormGroup.enable();
        mockStore.refreshState();

        expect(component.appFormGroup.enabled).toBeTrue();
    });

    it("should fetch settings for new statements", async () => {
        const dispatchSpy = spyOn(component.store, "dispatch");
        component.appForNewStatement = true;
        fixture.detectChanges();
        await fixture.whenStable();
        expect(dispatchSpy).toHaveBeenCalledWith(fetchSettingsAction());
    });

    it("should fetch contact details on value changes", async () => {
        const formValueChangesMock = new EventEmitter<any>();
        (component.appFormGroup as any).valueChanges = formValueChangesMock;
        component.task$ = of({statementId: undefined} as IAPIProcessTask);
        fixture.detectChanges();
        await fixture.whenStable();

        const value: Partial<IStatementInformationFormValue> = {
            contactId: "19191919"
        };
        const dispatchSpy = spyOn(component.store, "dispatch");
        component.appFormGroup.patchValue(value);
        formValueChangesMock.next(value);

        await timer(0).toPromise();

        expect(dispatchSpy).toHaveBeenCalledWith(fetchContactDetailsAction({contactId: value.contactId, statementId: undefined}));
    });

    it("should open contact data base module", async () => {
        const dispatchSpy = spyOn(component.store, "dispatch");
        component.openContactDataBaseModule();
        expect(dispatchSpy).toHaveBeenCalledWith(openContactDataBaseAction());
    });

    it("should search for new contacts", async () => {
        const dispatchSpy = spyOn(component.store, "dispatch");
        const options: IAPISearchOptions = {
            q: "",
            page: 0,
            size: 10
        };

        component.search();
        expect(dispatchSpy).toHaveBeenCalledWith(startContactSearchAction({options}));

        options.q = "191919";
        component.searchText = options.q;
        component.changePage(null);
        expect(dispatchSpy).toHaveBeenCalledWith(startContactSearchAction({options}));

        options.page = 19;
        component.changePage({page: options.page, size: options.size});
        expect(dispatchSpy).toHaveBeenCalledWith(startContactSearchAction({options}));
    });

    it("should mark all as touched when invalid form is submitted", async () => {
        component.appForNewStatement = true;
        fixture.detectChanges();
        await fixture.whenStable();

        const dispatchSpy = spyOn(component.store, "dispatch");

        expect(component.appFormGroup.touched).toBeFalse();
        expect(component.appFormGroup.invalid).toBeTrue();
        await component.submit();
        expect(component.appFormGroup.touched).toBeTrue();
        expect(dispatchSpy).toHaveBeenCalledWith(setErrorAction({
            statementId: "new",
            error: EErrorCode.MISSING_FORM_DATA
        }));
    });

    it("should submit information for a new statement", async () => {
        const value = createStatementInfoFormValue({
            title: "Title",
            creationDate: today,
            departmentsDueDate: today,
            dueDate: today,
            receiptDate: today,
            typeId: 3,
            city: "city",
            district: "district",
            contactId: "contactId",
            customerReference: ""
        });

        mockStore.overrideSelector(claimDetailsSelector, {19191919: {statementId: 19} as unknown as IAPIClaimDetails});

        component.appForNewStatement = true;
        fixture.detectChanges();
        await fixture.whenStable();

        component.appFormGroup.patchValue(value);
        fixture.detectChanges();
        await fixture.whenStable();

        const dispatchSpy = spyOn(component.store, "dispatch");

        expect(component.getValue()).toEqual(value);

        await component.submit(true);
        expect(dispatchSpy).toHaveBeenCalledWith(submitStatementInformationFormAction({
            new: true,
            value,
            responsible: true,
            customError: null
        }));

        await component.submit(false);
        expect(dispatchSpy).toHaveBeenCalledWith(submitStatementInformationFormAction({
            new: true,
            value,
            responsible: false,
            customError: null
        }));
    });

    it("should submit information for an existing statement", async () => {
        const value = createStatementInfoFormValue({
            title: "Title",
            departmentsDueDate: today,
            dueDate: today,
            receiptDate: today,
            creationDate: today,
            typeId: 3,
            city: "city",
            district: "district",
            contactId: "contactId"
        });

        mockStore.overrideSelector(claimDetailsSelector, {19191919: {statementId: 19} as unknown as IAPIClaimDetails});

        mockStore.overrideSelector(taskSelector, task as IAPIProcessTask);

        fixture.detectChanges();
        await fixture.whenStable();

        component.appFormGroup.patchValue(value);
        fixture.detectChanges();
        await fixture.whenStable();

        const dispatchSpy = spyOn(component.store, "dispatch");

        expect(component.getValue()).toEqual(value);

        await component.submit(true);
        expect(dispatchSpy).toHaveBeenCalledWith(submitStatementInformationFormAction({
            statementId: task.statementId,
            taskId: task.taskId,
            value,
            responsible: true,
            customError: null
        }));

        await component.submit(false);
        expect(dispatchSpy).toHaveBeenCalledWith(submitStatementInformationFormAction({
            statementId: task.statementId,
            taskId: task.taskId,
            value,
            responsible: false,
            customError: null
        }));
    });
});

describe("StatementInformationFormComponent2", () => {

    let component: StatementInformationFormComponent;
    let fixture: ComponentFixture<StatementInformationFormComponent>;
    const task: Partial<IAPIProcessTask> = {
        taskId: "19191919",
        statementId: 19
    };
    let mockStore: MockStore;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [
                I18nModule,
                StatementInformationFormModule
            ],
            providers: [
                provideMockStore({
                    initialState: {statements: {}, settings: {}, contacts: {}, attachments: {}},
                    selectors: [
                        {
                            selector: statementTypeOptionsSelector,
                            value: createSelectOptionsMock(5, "Statement Type")
                        },
                        {
                            selector: taskSelector,
                            value: task
                        },
                        {
                            selector: statementInfoSelector,
                            value: {title: "title"} as IAPIStatementModel
                        }
                    ]
                }),
                {
                    provide: APP_CONFIGURATION, useValue: {
                        claimDetails: {
                            displayClaimDetails: true,
                            pollClaimDetails: true
                        }
                    }
                }
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(StatementInformationFormComponent);
        mockStore = TestBed.inject(MockStore);
        component = fixture.componentInstance;
    });

    it("should dispatch fetchClaimDetailsAction on ngOnInit", async () => {

        component.appForNewStatement = false;

        const dispatchSpy = spyOn(mockStore, "dispatch");

        mockStore.overrideSelector(claimDetailsSelector, null);

        mockStore.overrideSelector(userNameSelector, "admin");

        await component.ngOnInit();

        expect(dispatchSpy).toHaveBeenCalled();
    });

    it("setErrorAndResetClaimDetails to reset current task", () => {
        component.claimLost = false;
        fixture.detectChanges();
        component.setErrorAndResetClaimDetails(task as IAPIProcessTask);
        expect(component.claimLost).toEqual(true);
    });

    it("should ask for confirmation before sending statement cancel", async () => {
        const confirmationSpy = spyOn(component.confirmService, "askForConfirmation").and.returnValue(true);
        await component.deleteStatement();
        expect(confirmationSpy).toHaveBeenCalled();
    });

    it("should dispatch cancelStatementAction on confirmation", async () => {
        const confirmationSpy = spyOn(component.confirmService, "askForConfirmation").and.returnValue(true);
        const dispatchSpy = spyOn(component.store, "dispatch");
        await component.deleteStatement();
        expect(confirmationSpy).toHaveBeenCalled();
        expect(dispatchSpy).toHaveBeenCalledWith(cancelStatementAction({statementId: task.statementId}));
    });

    it("should set typeoptions in initialization", async () => {
        component.appForNewStatement = true;
        mockStore.overrideSelector(statementTypesSelector, [{name: "label", id: 0}]);
        mockStore.overrideSelector(statementInfoSelector, {title: "title"} as IAPIStatementModel);
        mockStore.overrideSelector(taskSelector, task as IAPIProcessTask);
        await component.ngOnInit();
        expect(component.typeOptions.length).toEqual(1);
    });

    it("should set typeoptions in initialization with statement info set", async () => {
        mockStore.overrideSelector(statementTypesSelector, [{name: "label", id: 0}, {name: "label3", id: 3, deleted: true}]);
        mockStore.overrideSelector(statementInfoSelector, {title: "title", typeId: 3} as IAPIStatementModel);
        mockStore.overrideSelector(taskSelector, task as IAPIProcessTask);
        await component.ngOnInit();
        expect(component.typeOptions.length).toEqual(2);
    });
});

function createStatementInfoFormValue(value: Partial<IStatementInformationFormValue>): IStatementInformationFormValue {
    return {
        title: null,
        departmentsDueDate: null,
        dueDate: null,
        receiptDate: null,
        creationDate: null,
        typeId: null,
        city: null,
        district: null,
        contactId: null,
        customerReference: null,
        attachments: {
            add: [],
            edit: [],
            email: [],
            transferMailText: false,
            mailTextAttachmentId: null
        },
        sourceMailId: undefined,
        ...value,
    };
}
