/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {ComponentFixture, TestBed, waitForAsync} from "@angular/core/testing";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MockStore, provideMockStore} from "@ngrx/store/testing";
import {APP_CONFIGURATION} from "../../../../../core";
import {EAPIProcessTaskDefinitionKey, IAPIProcessTask} from "../../../../../core/api/process";
import {IAPIClaimDetails} from "../../../../../core/api/process/IAPIClaimDetails";
import {I18nModule} from "../../../../../core/i18n";
import {
    claimDetailsSelector,
    fetchClaimDetailsAction,
    setErrorAction,
    statementLoadingSelector,
    userNameSelector
} from "../../../../../store";
import {taskSelector} from "../../../../../store/process/selectors";
import {compileStatementArrangementAction, validateStatementArrangementAction} from "../../../../../store/statements/actions";
import {StatementEditorModule} from "../../statement-editor.module";
import {StatementEditorFormComponent} from "./statement-editor-form.component";

describe("StatementEditorFormComponent", () => {
    const statementId = 19;
    const taskId = "taskId";
    const task: IAPIProcessTask = {
        ...{} as IAPIProcessTask,
        taskId,
        statementId
    };

    let store: MockStore;
    let component: StatementEditorFormComponent;
    let fixture: ComponentFixture<StatementEditorFormComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [
                StatementEditorModule,
                BrowserAnimationsModule,
                I18nModule
            ],
            providers: [
                provideMockStore({
                    initialState: {},
                    selectors: [
                        {
                            selector: taskSelector,
                            value: task
                        },
                        {
                            selector: statementLoadingSelector,
                            value: false
                        }
                    ]
                }),
                {
                    provide: APP_CONFIGURATION, useValue: {
                        claimDetails: {
                            displayClaimDetails: true,
                            pollClaimDetails: true,
                            pollingIntervalInSeconds: 5
                        }
                    }
                }
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(StatementEditorFormComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        store = fixture.componentRef.injector.get(MockStore);
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should dispatch validateStatementArrangementAction on validate", async () => {
        const dispatchSpy = spyOn(store, "dispatch");
        await component.validate();
        expect(dispatchSpy).toHaveBeenCalledWith(
            validateStatementArrangementAction({statementId, taskId, arrangement: []}));
    });

    it("should dispatch compileStatementArrangementAction on compile", async () => {
        const dispatchSpy = spyOn(store, "dispatch");
        await component.compile();
        expect(dispatchSpy).toHaveBeenCalledWith(
            compileStatementArrangementAction({statementId, taskId, arrangement: []}));
    });

    it("should dispatch submitStatementEditorFormAction on submit", async () => {
        const dispatchSpy = spyOn(store, "dispatch");

        store.overrideSelector(claimDetailsSelector, { taskId: {statementId: 19} as unknown as IAPIClaimDetails});

        store.overrideSelector(taskSelector, {
            ...task,
            taskDefinitionKey: EAPIProcessTaskDefinitionKey.CHECK_AND_FORMULATE_RESPONSE
        });

        await component.submit();
        expect(dispatchSpy).toHaveBeenCalledTimes(4);

        await component.submit();
        expect(dispatchSpy).toHaveBeenCalledTimes(7);
    });

    it("should dispatch fetchClaimDetailsAction on ngOnInit", async () => {

        const dispatchSpy = spyOn(store, "dispatch");

        store.overrideSelector(claimDetailsSelector, { taskId: {statementId: 19} as unknown as IAPIClaimDetails});

        store.overrideSelector(userNameSelector, "admin");

        store.overrideSelector(taskSelector, {
            ...task,
            assignee: "someoneElse",
            taskDefinitionKey: EAPIProcessTaskDefinitionKey.CHECK_AND_FORMULATE_RESPONSE
        });

        await component.ngOnInit();

        expect(dispatchSpy).toHaveBeenCalledWith(
            fetchClaimDetailsAction({
                statementId: task.statementId,
                taskId: task.taskId,
            })
        );

        component.claimLost = true;

        await component.ngOnInit();

        expect(dispatchSpy).toHaveBeenCalledWith(
            fetchClaimDetailsAction({
                statementId: task.statementId,
                taskId: task.taskId
            })
        );
    });

    it("should dispatch fetchClaimDetailsAction when calling submit without params", async () => {

        const dispatchSpy = spyOn(store, "dispatch");

        store.overrideSelector(claimDetailsSelector, { 19: {statementId: 19} as unknown as IAPIClaimDetails});

        store.overrideSelector(userNameSelector, "admin");

        await component.submit();

        expect(dispatchSpy).toHaveBeenCalledWith(
            fetchClaimDetailsAction({statementId: task.statementId, taskId: task.taskId})
        );
    });

    it("setErrorAndResetClaimDetails to reset current task", () => {
        component.claimLost = false;
        fixture.detectChanges();
        component.setErrorAndResetClaimDetails(task);
        expect(component.claimLost).toEqual(true);
    });

    it("should clear errors on finalze", () => {
        const dispatchSpy = spyOn(store, "dispatch");
        store.overrideSelector(taskSelector, task);
        store.overrideSelector(statementLoadingSelector, false);
        component.finalize();
        expect(dispatchSpy).toHaveBeenCalledWith(
            setErrorAction({
                statementId: task.statementId,
                error: null
            })
        );
    });

});
