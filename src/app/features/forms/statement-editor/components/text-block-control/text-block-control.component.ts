/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, EventEmitter, forwardRef, Input, Output} from "@angular/core";
import {NG_VALUE_ACCESSOR} from "@angular/forms";
import {IAPITextArrangementItemModel, IAPITextBlockModel} from "../../../../../core";
import {AbstractControlValueAccessorDirective} from "../../../../../shared/controls/common";
import {ITextBlockRenderItem} from "../../../../../shared/text-block/model/ITextBlockRenderItem";
import {textToBlockDataArray} from "../../../../../shared/text-block/pipes/get-blockdata-array";
import {ITextblockError} from "../../pipes";

@Component({
    selector: "app-text-block-control",
    templateUrl: "./text-block-control.component.html",
    styleUrls: ["./text-block-control.component.scss"],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => TextBlockControlComponent),
            multi: true
        }
    ]
})
export class TextBlockControlComponent extends AbstractControlValueAccessorDirective<IAPITextArrangementItemModel> {

    @Input()
    public appTextBlockModel: IAPITextBlockModel;

    @Input()
    public appSelects: { [key: string]: string[] };

    @Input()
    public appReplacements: { [key: string]: string };

    @Input()
    public appErrors: ITextblockError[];

    @Output()
    public appRemove: EventEmitter<string> = new EventEmitter();

    @Output()
    public appAdd: EventEmitter<IAPITextArrangementItemModel> = new EventEmitter();

    public addPlaceholder(replacement: { name: string; newValue: string }) {
        this.writeValue({
            ...this.appValue,
            placeholderValues: {
                ...this.appValue?.placeholderValues,
                [replacement.name]: replacement.newValue
            }
        }, true);
    }

    private buildReplacementTextForBlock(blockData: ITextBlockRenderItem[]): string {
        let replacementText = "";
        for (const block of blockData) {
            if (block.type === "newline") {
                replacementText += "\n";
            } else if (block.type === "select") {
                replacementText += block.options[block.placeholder] ? block.options[block.placeholder] : "";
            } else {
                replacementText += block.placeholder ? block.placeholder : block.value;
            }
        }
        return replacementText;
    }

    public convertToFreeText() {
        let replacementText = "";
        const hasReplacementText = this.appValue?.replacement != null;
        if (!hasReplacementText) {
            const blockData = textToBlockDataArray(
                this.appTextBlockModel, this.appValue.placeholderValues, this.appReplacements, this.appSelects);
            replacementText = this.buildReplacementTextForBlock(blockData);
        } else {
            replacementText = this.appValue.replacement;
        }

        this.writeValue({
            ...this.appValue,
            replacement: replacementText
        }, true);
    }

    public setReplacement(text: string) {
        this.writeValue({
            ...this.appValue,
            replacement: text
        }, true);
    }

    public addNewLine() {
        this.appAdd.emit({type: "newline", placeholderValues: {}});
    }
}
