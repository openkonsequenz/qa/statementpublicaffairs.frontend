/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {RouterTestingModule} from "@angular/router/testing";
import {moduleMetadata, storiesOf} from "@storybook/angular";
import {I18nModule} from "../../../../../core/i18n";
import {StatementEditorModule} from "../../statement-editor.module";

const arrangements: any[] = [
    {type: "text", value: "Ein Text Test."},
    {type: "newline", value: ""},
    {type: "pagebreak", value: ""},
    {type: "text", value: "Ein weiterer Text."}
];


storiesOf("Features / Forms", module)
    .addDecorator(moduleMetadata({
        imports: [
            I18nModule,
            RouterTestingModule,
            StatementEditorModule,
            BrowserAnimationsModule
        ],
        providers: []
    }))
    .add("StatementPreviewComponent", () => ({
        template: `
            <app-statement-preview [appTextArrangements]="arrangements">
            </app-statement-preview>
        `,
        props: {
            arrangements
        }
    }));
