/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPITextArrangementErrorModel} from "../../../../core/api/text";
import {ErrorToMessagesPipe} from "./error-to-messages.pipe";

describe("ErrorToMessagesPipe", () => {

    const pipe = new ErrorToMessagesPipe();

    const error: IAPITextArrangementErrorModel = {
        arrangementId: 0,
        textblockId: "string",
        textblockGroup: "string",
        missingVariables: ["14", "15", "16"],
        requires: [{ids: ["5", "6", "7"], type: "and"}, {ids: ["8", "9", "10"], type: "or"}, {ids: ["11", "12", "13"], type: "xor"}],
        excludes: ["2", "3", "4"],
        after: "1"
    };

    it("should return an empty array if supplied with no data", () => {
        let result = pipe.transform(undefined);
        expect(result).toEqual([]);

        result = pipe.transform(null);
        expect(result).toEqual([]);
    });

    it("should convert the error model to an array of error messages in the right order", () => {
        const result = pipe.transform(error);

        expect(result).toBeTruthy();
        expect(Array.isArray(result)).toBeTrue();
        let ids = "";
        for (const errorMessage of result) {
            if (ids !== "") {
                ids += ",";
            }
            ids += errorMessage.ids.toString();
        }
        expect(ids).toEqual("1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16");
    });
});


