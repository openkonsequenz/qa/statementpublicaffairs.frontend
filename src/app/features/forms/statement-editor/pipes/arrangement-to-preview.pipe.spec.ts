/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {ITextBlockRenderItem} from "../../../../shared/text-block/model/ITextBlockRenderItem";
import {IStatementEditorControlConfiguration} from "../../../../store/statements/model";
import {ArrangementToPreviewPipe} from "./arrangement-to-preview.pipe";

describe("ArrangementToPreviewPipe", () => {

    const pipe = new ArrangementToPreviewPipe();

    const textItems: ITextBlockRenderItem[] = [
        {type: "text", value: "Ein Text mit folgendem zu ersetzenden Wert INSERT_VALUE INSERT_INPUT_VALUE"},
        {type: "text", value: "Freitext"},
        {type: "newline", value: ""},
        {type: "pagebreak", value: ""}
    ];

    const configurations: IStatementEditorControlConfiguration[] = [
        {
            textBlock: {
                id: "textBlockId",
                text: "Ein Text mit folgendem zu ersetzenden Wert <t:test> <f:testing>",
                excludes: [],
                requires: []
            },
            selects: {},
            replacements: {
                test: "INSERT_VALUE"
            },
            value: {
                type: "block",
                textblockId: "textBlockId",
                placeholderValues: {
                    "<f:testing>": "INSERT_INPUT_VALUE"
                }
            }
        },
        {
            selects: {},
            replacements: {},
            value: {
                type: "text",
                placeholderValues: {},
                replacement: "Freitext"
            }
        },
        {
            selects: {},
            replacements: {},
            value: {
                type: "newline",
                placeholderValues: {}
            }
        },
        {
            selects: {},
            replacements: {},
            value: {
                type: "pagebreak",
                placeholderValues: {}
            }
        }
    ];

    it("should return an empty array if supplied with no data", () => {
        let result = pipe.transform(undefined);
        expect(result).toEqual([]);

        result = pipe.transform(null);
        expect(result).toEqual([]);
    });

    it("should convert the array of configurations to an array of TextBlockRenderItems", () => {
        const result = pipe.transform(configurations);
        expect(result).toBeTruthy();
        // removes undefined properties
        expect(JSON.parse(JSON.stringify(result))).toEqual(JSON.parse(JSON.stringify(textItems)));
    });
});


