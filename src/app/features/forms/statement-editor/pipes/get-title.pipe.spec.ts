/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPITextArrangementItemModel} from "../../../../core/api/text";
import {GetTitlePipe} from "./get-title.pipe";

describe("GetTitlePipe", () => {

    const pipe = new GetTitlePipe();

    it("should return the correct title as string", () => {
        const arrangement: IAPITextArrangementItemModel = {
            type: "text",
            placeholderValues: {}
        };
        let result = pipe.transform(arrangement);
        expect(result).toEqual("textBlocks.standardBlocks.freeText");

        result = pipe.transform({...arrangement, type: "pagebreak"});
        expect(result).toEqual("textBlocks.standardBlocks.pagebreak");

        result = pipe.transform({...arrangement, type: "newline"});
        expect(result).toEqual("textBlocks.standardBlocks.newLine");

        result = pipe.transform({...arrangement, type: "block"});
        expect(result).toEqual(undefined);

        result = pipe.transform({...arrangement, type: "block", textblockId: "textBlockId"});
        expect(result).toEqual("textBlockId");

        result = pipe.transform(undefined);
        expect(result).toEqual(undefined);
    });
});


