/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Directive, Inject, OnDestroy} from "@angular/core";
import {select, Store} from "@ngrx/store";
import {combineLatest, interval, Observable, Subject} from "rxjs";
import {filter, take, takeUntil} from "rxjs/operators";
import {APP_CONFIGURATION, IAPIProcessTask, IAppConfiguration} from "../../../core";
import {IAPIClaimDetails} from "../../../core/api/process/IAPIClaimDetails";
import {
    claimDetailsSelector,
    deleteClaimDetailsAction,
    EErrorCode,
    fetchClaimDetailsAction,
    setErrorAction,
    statementLoadingSelector,
    taskSelector,
    userNameSelector
} from "../../../store";
import {TStoreEntities} from "../../../util";
import {AbstractReactiveFormDirective} from "./abstract-reactive-form.directive";

@Directive()
export abstract class AbstractEditorFormDirective<T extends object>
    extends AbstractReactiveFormDirective<T>
    implements OnDestroy {

    public claimDetails$: Observable<TStoreEntities<IAPIClaimDetails>> = this.store.pipe(select(claimDetailsSelector));

    public initializing = true;

    public claimLost: boolean;

    public userName$ = this.store.pipe(select(userNameSelector));

    public task$ = this.store.pipe(select(taskSelector));

    public isStatementLoading$ = this.store.pipe(select(statementLoadingSelector));

    protected unsubscribeClaimDetails$ = new Subject<void>();

    public constructor(
        public store: Store,
        @Inject(APP_CONFIGURATION) public configuration: IAppConfiguration
    ) {
        super();
    }

    public ngOnDestroy() {
        super.ngOnDestroy();
    }

    public setErrorAndResetClaimDetails(task: IAPIProcessTask) {
        this.unsubscribeClaimDetails$.next();
        this.store.dispatch(setErrorAction({
            error: EErrorCode.TASK_NOT_CLAIMED_BY_USER,
            statementId: task.statementId,
            setBothErrors: true
        }));
        this.store.dispatch(deleteClaimDetailsAction({
            taskId: task.taskId
        }));
        this.claimLost = true;
    }

    protected async readClaimDetails() {
        combineLatest([this.claimDetails$, this.userName$, this.task$]).pipe(
            filter(([claimDetails, loggedInUsername, task]) =>
                claimDetails != null && task?.taskId != null &&
                claimDetails[task.taskId] != null &&
                (claimDetails[task.taskId].assignee !== loggedInUsername || claimDetails[task.taskId].current !== true)
            ),
            takeUntil(this.unsubscribeClaimDetails$),
            takeUntil(this.destroy$)
        ).subscribe(async ([claimDetails, loggedInUsername, task]) => {
            const isLoading = await this.isStatementLoading$.pipe(take(1)).toPromise();
            if (!isLoading && !this.initializing) {
                this.setErrorAndResetClaimDetails(task);
            }
        });

        await this.fetchClaimDetails();

        const intervalInSeconds = this.configuration.claimDetails.pollingIntervalInSeconds ?? 2;
        interval(intervalInSeconds * 1000).pipe(
            takeUntil(this.unsubscribeClaimDetails$),
            takeUntil(this.destroy$)
        ).subscribe(() => {
            this.fetchClaimDetails();
        });
    }

    protected async fetchClaimDetails() {
        const task = await this.task$.pipe(take(1)).toPromise();
        this.store.dispatch(fetchClaimDetailsAction({statementId: task.statementId, taskId: task.taskId}));
    }
}
