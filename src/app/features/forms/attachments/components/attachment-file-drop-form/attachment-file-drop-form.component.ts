/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, Input} from "@angular/core";
import {IAPIAttachmentTag} from "../../../../../core/api/attachments";
import {createAttachmentForm, IAttachmentControlValue, IAttachmentFormValue} from "../../../../../store";
import {arrayJoin} from "../../../../../util/store";
import {AbstractReactiveFormArrayDirective} from "../../../abstract";

@Component({
    selector: "app-attachment-file-drop-form",
    templateUrl: "./attachment-file-drop-form.component.html",
    styleUrls: ["./attachment-file-drop-form.component.scss"]
})
export class AttachmentFileDropFormComponent extends AbstractReactiveFormArrayDirective<IAttachmentControlValue> {

    @Input()
    public appTitle: string;

    @Input()
    public appAutoTagIds: string[];

    @Input()
    public appTagList: IAPIAttachmentTag[];

    @Input()
    public appFormGroup = createAttachmentForm();

    @Input()
    public appFormArrayName: keyof IAttachmentFormValue | "considerations" = "add";

    public addControlForFiles(files: File[]) {
        arrayJoin(files).forEach((file) => {
            this.addControl({
                name: file.name,
                file,
                tagIds: arrayJoin(this.appAutoTagIds)
            });
        });
    }

}
