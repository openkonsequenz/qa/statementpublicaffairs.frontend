/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/


import {IAPIAttachmentModel} from "../../../../core/api/attachments";
import {IAPIEmailAttachmentModel} from "../../../../core/api/mail";
import {GetMailAttachmentsPipe} from "./get-mail-attachments.pipe";

describe("GetMailAttachmentsPipe", () => {

    const pipe = new GetMailAttachmentsPipe();

    describe("transform", () => {

        it("should return an empty array if called with empty lists", () => {
            let result = pipe.transform(null, null);
            expect(result).toEqual([]);
            result = pipe.transform([], []);
            expect(result).toEqual([]);
        });

        it("should return list of mail attachments with selected being the ones already in statement attachments", () => {
            const attachments: IAPIAttachmentModel[] = [
                {name: "attachment1", tagIds: []},
                {name: "attachment2", tagIds: ["email"]},
                {name: "mailText.txt", tagIds: ["email", "email-text"]}
            ] as IAPIAttachmentModel[];
            const emailAttachments: IAPIEmailAttachmentModel[] = [
                {name: "attachment2"},
                {name: "attachment3"}
            ] as IAPIEmailAttachmentModel[];
            const result = pipe.transform(attachments, emailAttachments);
            expect(result).toEqual([
                {name: "attachment2", tagIds: ["email"], isSelected: true},
                {name: "attachment3", tagIds: [], isSelected: false}
            ]);
        });
    });
});

