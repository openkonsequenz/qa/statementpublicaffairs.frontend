/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPIAttachmentTag} from "../../../../core/api/attachments";
import {FilterOutTagsPipe} from "./filter-out-tags.pipe";

describe("FilterOutTagsPipe", () => {

    const pipe = new FilterOutTagsPipe();

    describe("transform", () => {

        it("should return the mail text attachment with name set to mail subject", () => {

            const tags: IAPIAttachmentTag[] = [
                {label: "label", id: "0"},
                {label: "label1", id: "1"},
                {label: "label2", id: "2"},
            ];

            let filteredTags = pipe.transform(tags, []);
            expect(filteredTags).toEqual(tags);

            filteredTags = pipe.transform(tags, ["1"]);
            expect(filteredTags).toEqual(tags.filter((tag) => tag.id !== "1"));
        });
    });
});

