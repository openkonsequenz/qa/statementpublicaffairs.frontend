/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {ChangeDetectionStrategy, Component, EventEmitter, Inject, Input, Output} from "@angular/core";
import {EAPIUserRoles, HELP_ROUTE} from "../../../../core";
import {arrayJoin} from "../../../../util/store";

export interface INavHeaderRoute {
    icon: string;
    link: string;
    tooltip?: string;
    exact?: boolean;
    roles?: EAPIUserRoles[];
    target?: string;
    largeIcon?: boolean;
}

@Component({
    selector: "app-nav-header",
    templateUrl: "./nav-header.component.html",
    styleUrls: ["./nav-header.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavHeaderComponent {

    @Input()
    public appCurrentUrl: string;

    @Input()
    public appMenuHidden = false;

    @Input()
    public appUserName: string;

    @Input()
    public appUserRoles: EAPIUserRoles[];

    @Output()
    public readonly appLogout = new EventEmitter<void>();

    public appRoutes: INavHeaderRoute[] = [
        {
            icon: "home",
            link: "/",
            tooltip: "core.header.home",
            exact: true
        },
        {
            icon: "find_in_page",
            link: "/search/list",
            tooltip: "core.header.search"
        },
        {
            icon: "map",
            link: "/search/map",
            tooltip: "core.header.searchMap"
        },
        {
            icon: "email",
            link: "/mail",
            tooltip: "core.header.mail",
            roles: [EAPIUserRoles.SPA_OFFICIAL_IN_CHARGE]
        },
        {
            icon: "note_add",
            link: "/new",
            tooltip: "core.header.new",
            roles: [EAPIUserRoles.SPA_OFFICIAL_IN_CHARGE]
        },
        {
            icon: "settings",
            link: "/settings",
            tooltip: "core.header.settings",
            roles: [EAPIUserRoles.SPA_OFFICIAL_IN_CHARGE, EAPIUserRoles.SPA_ADMIN]
        }
    ];

    public constructor(@Inject(HELP_ROUTE) public helpRoute: string) {
        console.log("helpRoute:" + helpRoute);
    }

    public logOut() {
        this.appLogout.emit();
    }

    public isLinkDisplayed(roles: EAPIUserRoles[]) {
        return arrayJoin(roles).length === 0 || roles.some((_) => arrayJoin(this.appUserRoles).indexOf(_) > -1);
    }

    public isLinkActive(link: string, exact?: boolean) {
        if (this.appCurrentUrl == null) {
            return false;
        }
        const currentUrl = this.appCurrentUrl.toLowerCase();
        link = link.toLowerCase();
        return exact ? currentUrl === link : currentUrl.startsWith(link);
    }

}
