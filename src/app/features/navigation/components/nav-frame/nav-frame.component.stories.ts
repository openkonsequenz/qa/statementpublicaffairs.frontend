/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {RouterTestingModule} from "@angular/router/testing";
import {action} from "@storybook/addon-actions";
import {boolean, select, text, withKnobs} from "@storybook/addon-knobs";
import {moduleMetadata, storiesOf} from "@storybook/angular";
import {I18nModule} from "../../../../core";
import {EExitCode} from "../../../../store";
import {AppNavigationFrameModule} from "../../app-navigation-frame.module";

const template = `
    <app-nav-frame
        (appCloseWindow)="closeWindow()"
        (appLogout)="appLogOut()"
        [appExitCode]="showExitPage ? appExitCode : undefined"
        [appIsLoading]="appIsLoading"
        [appMenuHidden]="appMenuHidden"
        [appPortalRoute]="appPortalRoute"
        [appUserName]="appUserName"
        [appVersion]="appVersion">
        <div style="box-sizing: border-box; width: 100%;"
            [style.height]="withOverflow ? '120vh' : '100%'">
        </div>
    </app-nav-frame>
`;

storiesOf("Features / 01 Navigation", module)
    .addDecorator(withKnobs)
    .addDecorator(moduleMetadata({
        imports: [
            RouterTestingModule,
            I18nModule,
            AppNavigationFrameModule
        ]
    }))
    .add("NavFrameComponent", () => ({
        template,
        props: {
            // Inputs
            appExitCode: select("Exit Code", EExitCode, EExitCode.LOGOUT),
            appIsLoading: boolean("Loading", false),
            appMenuHidden: boolean("Menu Hidden", false),
            appPortalRoute: text("Portal Route", "/"),
            appUserName: text("User", "Hugo Haferkamp"),
            appVersion: text("Version", "1.2.3/1.2.3"),
            showExitPage: boolean("showExitPage", false),
            withOverflow: boolean("Overflow", false),
            // Outputs
            appCloseWindow: action("appCloseWindow"),
            appLogOut: action("appLogOut")
        }
    }));
