/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, OnDestroy, OnInit} from "@angular/core";
import {select, Store} from "@ngrx/store";
import {Subject} from "rxjs";
import {map, takeUntil} from "rxjs/operators";
import {
    getAttachmentTagsSelector,
    getSettingsLoadingSelector,
    statementTypesSelector,
    submitTagsAction,
    submitTypesAction
} from "../../../../store";

@Component({
    selector: "app-tag-settings",
    templateUrl: "./documents-settings.component.html",
    styleUrls: ["./documents-settings.component.scss"]
})
export class DocumentsSettingsComponent implements OnInit, OnDestroy {

    public tags$ = this.store.pipe(select(getAttachmentTagsSelector)).pipe(
        map((tags) => tags
            .filter((_) => !_.disabled)
        )
    );

    public types$ = this.store.pipe(select(statementTypesSelector)).pipe(
        map((types) => types
            .filter((_) => !_.disabled)
        )
    );

    public loading$ = this.store.pipe(select(getSettingsLoadingSelector));

    public tagList: { label: string; add?: boolean; delete?: boolean; initial?: boolean; isStandard?: boolean }[] = [];

    public typeList: { label: string; add?: boolean; delete?: boolean; initial?: boolean }[] = [];

    private destroy$ = new Subject<void>();

    public constructor(private readonly store: Store) {
    }

    public ngOnInit() {
        this.tags$.pipe(
            takeUntil(this.destroy$)
        ).subscribe((tags) => {
            this.tagList = tags.map((_) => ({
                label: _.label,
                initial: true,
                isStandard: _.standard
            }));
        });
        this.types$.pipe(
            takeUntil(this.destroy$)
        ).subscribe((tags) => {
            this.typeList = tags.map((_) => ({
                label: _.name,
                initial: true,
            }));
        });
    }

    public async ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    public addTag(tagLabel: string) {
        if (tagLabel) {
            this.tagList.push({label: tagLabel, add: true});
        }
    }

    public deleteTag(index: number) {
        const tag = this.tagList[index];
        if (tag == null || tag?.isStandard) {
            return;
        }
        if (tag.initial) {
            tag.delete = true;
        } else {
            this.tagList.splice(index, 1);
        }
    }

    public restoreTag(index: number) {
        const tag = this.tagList[index];
        if (tag == null) {
            return;
        }
        tag.delete = false;
    }

    public addType(label: string) {
        if (label) {
            this.typeList.push({label, add: true});
        }
    }

    public deleteType(index: number) {
        const type = this.typeList[index];
        if (type == null) {
            return;
        }
        if (type.initial) {
            type.delete = true;
        } else {
            this.typeList.splice(index, 1);
        }
    }

    public restoreType(index: number) {
        const type = this.typeList[index];
        if (type == null) {
            return;
        }
        type.delete = false;
    }

    public save() {
        const tagLabelsToSubmit = this.tagList
            .filter((tag) => tag.add != null || tag.delete != null)
            .map((_) => ({label: _.label, add: _.add, delete: _.delete}));

        const typeLabelsToSubmit = this.typeList
            .filter((type) => type.add != null || type.delete != null)
            .map((_) => ({label: _.label, add: _.add, delete: _.delete}));

        if (tagLabelsToSubmit.length > 0) {
            this.store.dispatch(submitTagsAction({tags: tagLabelsToSubmit}));
        }
        if (typeLabelsToSubmit.length > 0) {
            this.store.dispatch(submitTypesAction({types: typeLabelsToSubmit}));
        }
    }

}
