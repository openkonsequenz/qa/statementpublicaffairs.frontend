/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {AdminRouteGuardService, OfficialInChargeOrAdminRouteGuardService} from "../../store";
import {DepartmentsSettingsComponent, DepartmentsSettingsModule} from "./departments";
import {DocumentsSettingsComponent, DocumentsSettingsModule} from "./documents";
import {TextBlockSettingsModule, TextBlocksSettingsComponent} from "./text-blocks";
import {UsersSettingsComponent, UsersSettingsModule} from "./users";

const routes: Routes = [
    {
        path: "",
        pathMatch: "full",
        redirectTo: "text-blocks"
    },
    {
        path: "departments",
        pathMatch: "full",
        component: DepartmentsSettingsComponent,
        canActivate: [AdminRouteGuardService]
    },
    {
        path: "documents",
        pathMatch: "full",
        component: DocumentsSettingsComponent,
        canActivate: [AdminRouteGuardService]
    },
    {
        path: "text-blocks",
        pathMatch: "full",
        component: TextBlocksSettingsComponent,
        canActivate: [OfficialInChargeOrAdminRouteGuardService]
    },
    {
        path: "users",
        pathMatch: "full",
        component: UsersSettingsComponent,
        canActivate: [AdminRouteGuardService]
    }
];

@NgModule({
    imports: [
        DepartmentsSettingsModule,
        DocumentsSettingsModule,
        TextBlockSettingsModule,
        UsersSettingsModule,
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class SettingsRoutingModule {

}
