/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {provideMockStore} from "@ngrx/store/testing";
import {boolean, withKnobs} from "@storybook/addon-knobs";
import {moduleMetadata, storiesOf} from "@storybook/angular";
import {EAPIUserRoles, I18nModule, IAPIUserInfoExtended} from "../../../../../core";
import {UsersSettingsModule} from "../../users-settings.module";

const oicUser: IAPIUserInfoExtended = {
    id: 1,
    settings: {
        email: "email",
        phone: "0430-45445",
        fax: "0430-45445fax",
        initials: "hug",
        departments: []
    },
    firstName: "hugo",
    lastName: "hugo",
    roles: [
        EAPIUserRoles.SPA_OFFICIAL_IN_CHARGE
    ],
    userName: "oic"
};

const divisionMemberUser: IAPIUserInfoExtended = {
    id: 2,
    settings: {
        email: "dmemail",
        phone: "dm0430-45445",
        fax: "dm0430-45445fax",
        initials: "dm",
        departments: []
    },
    firstName: "division",
    lastName: "member",
    roles: [
        EAPIUserRoles.DIVISION_MEMBER
    ],
    userName: "dm"
};

const adminUser: IAPIUserInfoExtended = {
    ...oicUser,
    roles: [
        EAPIUserRoles.SPA_ADMIN
    ]
};

storiesOf("Features / Settings / Users", module)
    .addDecorator(withKnobs)
    .addDecorator(moduleMetadata({
        providers: [
            provideMockStore()
        ],
        imports: [
            I18nModule,
            UsersSettingsModule
        ]
    }))
    .add("UsersSettingsEdit", () => ({
        template: `
            <app-users-settings-edit [appSelectedUser]="appSelectedUser">
            </app-users-settings-edit>
        `,
        props: {
            appDisabled: boolean("appDisabled", false),
            appSelectedUser: oicUser
        }
    }));
