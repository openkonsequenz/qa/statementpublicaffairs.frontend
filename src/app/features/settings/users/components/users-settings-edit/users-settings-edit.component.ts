/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from "@angular/core";
import {FormArray, FormControl, FormGroup, ValidatorFn, Validators} from "@angular/forms";
import {IAPIUserInfoExtended, IAPIUserSettings} from "../../../../../core";
import {EErrorCode} from "../../../../../store";
import {createFormGroup} from "../../../../../util";

export interface IUserSettingsEditFormValue {
    email: string;
    phone?: string;
    fax?: string;
    initials?: string;
    departments: { name: string; group: string; standIn?: boolean }[];
}

@Component({
    selector: "app-users-settings-edit",
    templateUrl: "./users-settings-edit.component.html",
    styleUrls: ["./users-settings-edit.component.scss"]
})
export class UsersSettingsEditComponent implements OnChanges {

    @Input()
    public appSelectedUser: IAPIUserInfoExtended;

    @Input()
    public appDisabled: boolean;

    @Input()
    public appDepartments: { key: string; value: string[] }[];

    @Output()
    public appSubmit = new EventEmitter<{ id: number; value: IAPIUserSettings }>();

    @Output()
    public appError = new EventEmitter<EErrorCode>();

    public formGroup;

    public constructor() {
        this.formGroup = createFormGroup<IUserSettingsEditFormValue>({
            email: new FormControl("", [Validators.email]),
            phone: new FormControl("", []),
            fax: new FormControl("", []),
            initials: new FormControl("", []),
            departments: new FormArray([])
        }, [this.departmentSingleUse]);
    }

    public get departments() {
        return this.formGroup.get("departments") as FormArray;
    }

    public patchValue(value: Partial<IAPIUserSettings>) {
        this.formGroup.patchValue({...value});
    }

    public patchValueForDepartment(index: number, value: Partial<{ name: string; group: string; standIn?: boolean }>) {
        const formArray = this.formGroup.get("departments") as FormArray;
        formArray.controls[index].patchValue({...value});
    }

    public getValue(): IUserSettingsEditFormValue {
        return this.formGroup.value;
    }

    public ngOnChanges(changes: SimpleChanges) {
        const onChange = (keys: Array<keyof UsersSettingsEditComponent>, fn: () => any) => {
            if (keys.some((key) => changes[key])) {
                fn();
            }
        };
        onChange(["appSelectedUser"], () => {
            this.patchValue({
                email: this.appSelectedUser?.settings?.email,
                phone: this.appSelectedUser?.settings?.phone,
                fax: this.appSelectedUser?.settings?.fax,
                initials: this.appSelectedUser?.settings?.initials
            });
            const formArray = this.formGroup.get("departments") as FormArray;
            formArray.clear();
            this.appSelectedUser?.settings?.departments.forEach((department) => {
                formArray.push(
                    new FormGroup({
                        name: new FormControl(department.name),
                        group: new FormControl(department.group),
                        standIn: new FormControl({value: department.standIn, disabled: this.appDisabled})
                    })
                );
            });
            if (formArray.controls.length === 0) {
                this.addDepartmentEntry();
            }
        });
        onChange(["appDisabled"], () => this.appDisabled ? this.formGroup.disable() : this.formGroup.enable());
    }

    public submit(id: number) {
        const {email, phone, fax, initials, departments} = {...this.getValue()};
        this.formGroup.markAsTouched();

        for (const department of departments) {
            if (department.group != null && department.name == null) {
                this.appError.emit(EErrorCode.MISSING_FORM_DATA);
                return;
            }
        }

        if (this.formGroup.invalid) {
            this.appError.emit(this.formGroup.errors?.departmentUsedMultipleTimes
                ? EErrorCode.DEPARTMENT_MULTIPLE_USE
                : EErrorCode.BAD_USER_DATA);
            return;
        }

        const newDepartments = departments.filter((department) => department.group != null && department.name != null);

        this.appSubmit.emit({
            id,
            value: {
                email,
                phone,
                fax,
                initials,
                departments: newDepartments
            }
        });
    }

    public addDepartmentEntry() {
        this.departments.push(
            new FormGroup({
                name: new FormControl(null),
                group: new FormControl(null),
                standIn: new FormControl({value: false, disabled: this.appDisabled})
            })
        );
    }

    public deleteDepartmentEntry(index: number) {
        if (index > 0 && index < this.departments.length) {
            this.departments.removeAt(index);
        }
    }

    private departmentSingleUse: ValidatorFn = (formGroup: FormGroup) => {
        const departments = formGroup.get("departments").value;
        for (const department of departments) {
            if (departments.filter((d) => d.name != null && d.group === department.group && d.name === department.name).length > 1) {
                return {departmentUsedMultipleTimes: true};
            }
        }
        return null;
    };

    public deleteEmail(id: number) {
        this.formGroup.patchValue({
            email: ""
        });
        this.submit(id);
    }
}
