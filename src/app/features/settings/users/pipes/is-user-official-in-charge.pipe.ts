/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Pipe, PipeTransform} from "@angular/core";
import {EAPIUserRoles, IAPIUserInfoExtended} from "../../../../core";

@Pipe({
    name: "appIsUserOfficialInCharge"
})
export class IssUserOfficialInChargePipe implements PipeTransform {

    public transform(user: IAPIUserInfoExtended): boolean {
        return user?.roles?.find((_) => _ === EAPIUserRoles.SPA_OFFICIAL_IN_CHARGE) != null;
    }

}

