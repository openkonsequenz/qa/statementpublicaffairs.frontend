/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {EAPIUserRoles, IAPIUserInfoExtended, UserStates} from "../../../../core";
import {FilterUserListPipe} from "./filter-user-list.pipe";

describe("FilterUserListPipe", () => {
    const pipe = new FilterUserListPipe();
    const departmentName = "departmentName";
    const departmentGroupName = "departmentGroupName";
    const userList: IAPIUserInfoExtended[] = [
        createUserObject(17, EAPIUserRoles.SPA_ADMIN),
        createUserObject(18, EAPIUserRoles.SPA_OFFICIAL_IN_CHARGE),
        createUserObject(19, EAPIUserRoles.DIVISION_MEMBER, departmentGroupName, departmentName),
        createUserObject(20, null, null, null, false)
    ];

    it("should filter the user list", () => {
        expect(pipe.transform(userList)).toEqual(userList);
        expect(pipe.transform(userList, {q: "19"})).toEqual([userList[2]]);
        expect(pipe.transform(userList, {role: EAPIUserRoles.DIVISION_MEMBER})).toEqual([userList[2]]);
        expect(pipe.transform(userList, {departmentName})).toEqual([userList[2]]);
        expect(pipe.transform(userList, {departmentGroupName})).toEqual([userList[2]]);
        expect(pipe.transform(userList, {state: UserStates.DELETED})).toEqual([userList[3]]);
        expect(pipe.transform(userList, {state: UserStates.ACTUAL}).length).toEqual(3);
    });

});

function createUserObject(id: number,
                          role: EAPIUserRoles,
                          departmentGroupName?: string,
                          departmentName?: string,
                          isKeyCloakUser: boolean = true): IAPIUserInfoExtended {
    return {
        ...{} as IAPIUserInfoExtended,
        id,
        roles: role ? [role] : null,
        settings: {
            email: "test@email" + id + ".org",
            departments: departmentName == null ? [] : [{name: departmentName, group: departmentGroupName}]
        },
        isKeyCloakUser
    };
}
