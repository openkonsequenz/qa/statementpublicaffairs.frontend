/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, Input} from "@angular/core";
import {Router} from "@angular/router";

interface ISettingsSideMenuEntry {
    label: string;
    icon: string;
    routerLink: string;
}

@Component({
    selector: "app-settings-side-menu",
    templateUrl: "./settings-side-menu.component.html",
    styleUrls: ["./settings-side-menu.component.scss"]
})
export class SettingsSideMenuComponent {

    @Input()
    public appForAdmin: boolean;

    public defaultEntries: ISettingsSideMenuEntry[] = [
        {
            label: "settings.textBlocks.title",
            icon: "subject",
            routerLink: "/settings/text-blocks"
        }
    ];

    public adminEntries: ISettingsSideMenuEntry[] = [
        {
            label: "settings.textBlocks.title",
            icon: "subject",
            routerLink: "/settings/text-blocks"
        },
        {
            label: "settings.documents.title",
            icon: "description",
            routerLink: "/settings/documents"
        },
        {
            label: "settings.departments.title",
            icon: "work",
            routerLink: "/settings/departments"
        },
        {
            label: "settings.users.title",
            icon: "supervisor_account",
            routerLink: "/settings/users"
        }
    ];

    constructor(public router: Router) {

    }

}
