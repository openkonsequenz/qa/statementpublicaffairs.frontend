/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, OnDestroy, OnInit} from "@angular/core";
import {FormGroup} from "@angular/forms";
import {select, Store} from "@ngrx/store";
import {TranslateService} from "@ngx-translate/core";
import {defer, merge, Observable, of, Subject} from "rxjs";
import {map, switchMap, takeUntil} from "rxjs/operators";
import {
    fetchTextblockSettingsAction,
    getSettingsLoadingSelector,
    getTextblockSettingsSelector,
    isAdminSelector,
    submitTextblockSettingsAction
} from "../../../../store";
import {TextBlockSettingsForm} from "../TextBlockSettingsForm";
import {ITextBlockFormTextControlButton} from "./text-block-form";


export interface ITextBlockSettingConfig {
    selection?: {
        groupIndex: number;
        index: number;
        control: FormGroup;
    };
    negative?: boolean;
}

@Component({
    selector: "app-text-block-settings",
    templateUrl: "./text-blocks-settings.component.html",
    styleUrls: ["./text-blocks-settings.component.scss"],
    providers: [
        {
            provide: TextBlockSettingsForm,
            useValue: new TextBlockSettingsForm()
        }
    ]
})
export class TextBlocksSettingsComponent implements OnInit, OnDestroy {

    public loading$ = this.store.pipe(select(getSettingsLoadingSelector));

    public textBlockConfig$ = this.store.pipe(select(getTextblockSettingsSelector));

    public isAdmin$ = this.store.pipe(select(isAdminSelector));

    public selectKeys$ = defer(() => merge(of(0), this.form.selects.valueChanges)).pipe(
        map(() => this.form.getSelectKeys())
    );

    public replacementKeys = [
        "id",
        "title",
        "city",
        "district",
        "locationDesignation",
        "cityWithLocationDesignation",
        "sectors",
        "creationDate",
        "receiptDate",
        "dueDate",
        "customerReference",
        "sachbearbeiter",
        "c-greeting",
        "c-firstName",
        "c-lastName",
        "c-salutation",
        "c-title"
    ];

    public additionalReplacementKeys = [
        "c-community",
        "c-communitySuffix",
        "c-company",
        "c-email",
        "c-houseNumber",
        "c-postCode",
        "c-street",
        "departmentsDueDate",
        "oic-initials",
        "oic-fax",
        "oic-phone",
        "oic-email",
        "oic-name"
    ];

    public replacements$: Observable<{ [key: string]: string }> = of([...this.replacementKeys, ...this.additionalReplacementKeys]).pipe(
        switchMap((keys) => this.translateKeys(keys, "settings.textBlocks.replacements."))
    );

    public defaultButtons$: Observable<ITextBlockFormTextControlButton[]> = of(["freeText", "date"]).pipe(
        switchMap((keys) => this.translateKeys(keys, "settings.textBlocks.replacements.")),
        map((obj: { freeText: string; date: string }) => [
            {
                icon: "format_bold",
                token: "**",
                startToken: "**"
            },
            {
                icon: "format_italic",
                token: "__",
                startToken: "__"
            },
            {
                icon: "format_list_bulleted",
                startToken: "* ",
                token: "",
                requireLineBreak: true
            },
            {
                label: obj.freeText,
                icon: "edit",
                token: `<f:${obj.freeText}>`
            },
            {
                label: obj.date,
                icon: "today",
                token: `<d:${obj.date}>`
            }
        ])
    );

    public replacementButtons$: Observable<ITextBlockFormTextControlButton[]> = of(this.replacementKeys).pipe(
        switchMap((keys) => this.translateKeys(keys, "settings.textBlocks.replacements.")),
        map((obj) => this.replacementKeys.map((key) => ({label: obj[key], token: `<t:${key}>`})))
    );

    public selectButtons$: Observable<ITextBlockFormTextControlButton[]> = defer(() => this.selectKeys$).pipe(
        map((keys) => keys.map((key) => ({label: key, token: `<s:${key}>`, icon: "play_arrow", rotateIcon: true})))
    );

    public config: ITextBlockSettingConfig[] = [{}, {negative: true}];

    private destroy$ = new Subject<void>();

    constructor(public store: Store, public translateService: TranslateService, public form: TextBlockSettingsForm) {

    }

    public ngOnInit() {
        this.store.dispatch(fetchTextblockSettingsAction());
        this.textBlockConfig$.pipe(takeUntil(this.destroy$)).subscribe((value) => {
            this.form.setForm(value);
            this.config
                .filter((_) => _.selection != null)
                .forEach((_) => this.selectTextBlock(_.selection.groupIndex, _.selection.index, _.negative));
        });
        this.loading$.pipe(takeUntil(this.destroy$)).subscribe((loading) => this.form.disable(loading));
    }

    public ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    public submit() {
        this.store.dispatch(submitTextblockSettingsAction({data: this.form.getValue()}));
    }

    public trackByIndex(index: number) {
        return index;
    }

    public selectTextBlock(groupIndex: number, index: number, negative?: boolean) {
        const control = this.form.getTextBlockFormArrayForGroup(groupIndex, negative)?.at(index);
        this.config.find((_) => _.negative === negative).selection
            = control instanceof FormGroup ? {groupIndex, index, control} : undefined;
    }


    public changeGroupName(groupIndex: number, groupName: string, negative?: boolean) {
        this.form.getTextBlockGroups(negative).at(groupIndex)?.patchValue({groupName});
    }

    public insertTextBlockGroup(groupIndex: number, negative?: boolean) {
        this.form.insertGroup(groupIndex, negative);
        this.insertTextBlock(groupIndex, 0, negative);
    }

    public deleteTextBlockGroup(groupIndex: number, negative?: boolean) {
        const length = this.form.removeGroup(groupIndex, negative);
        if (length === 0) {
            this.insertTextBlockGroup(0, negative);
        }
        this.deselect();
    }


    public insertTextBlock(groupIndex: number, index: number, negative?: boolean) {
        this.form.insertTextBlock(groupIndex, index, negative);
        this.selectTextBlock(groupIndex, index, negative);
    }

    public moveTextBlock(groupIndex: number, from: number, to: number, negative: boolean) {
        this.form.moveTextBlockInGroup(groupIndex, from, to, negative);
    }

    public deleteTextBlock(id: string, groupIndex: number, index: number, negative?: boolean) {
        const length = this.form.removeTextBlock(groupIndex, index, negative);
        if (length === 0) {
            this.insertTextBlock(groupIndex, 0, negative);
        }
        this.selectTextBlock(groupIndex, index, negative);
    }

    /**
     * Returns an object, where each key from a given list has its translation as value.
     * @param keys List of keys which are translated.
     * @param translationKeyPrefix Prefix which is added to all keys prior translation (but is not part of any key in the result).
     */
    private translateKeys(keys: string[], translationKeyPrefix: string): Observable<{ [key: string]: string }> {
        const translationKeys = keys.map((key) => translationKeyPrefix + key);
        return this.translateService.get(translationKeys).pipe(
            map((translation) => keys.reduce((replacement, key, index) => ({
                ...replacement,
                [key]: translation[translationKeys[index]]
            }), {}))
        );
    }

    private deselect(negative?: boolean) {
        this.config.find((_) => _.negative === negative).selection = undefined;
    }

}
