/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {DragDropModule} from "@angular/cdk/drag-drop";
import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {MatIconModule} from "@angular/material/icon";
import {TranslateModule} from "@ngx-translate/core";
import {CommonControlsModule} from "../../../shared/controls/common";
import {SelectModule} from "../../../shared/controls/select";
import {ActionButtonModule} from "../../../shared/layout/action-button";
import {AutoFocusAfterInitModule} from "../../../shared/layout/auto-focus-after-init";
import {CollapsibleModule} from "../../../shared/layout/collapsible";
import {SideMenuModule} from "../../../shared/layout/side-menu";
import {SharedPipesModule} from "../../../shared/pipes";
import {TextBlockModule} from "../../../shared/text-block";
import {SharedSettingsModule} from "../shared";
import {TextBlocksSettingsComponent} from "./components";
import {TextBlockFormComponent} from "./components/text-block-form";
import {TextBlockListFormComponent} from "./components/text-block-list-form";
import {TextBlockSelectFormComponent} from "./components/text-block-select-form";
import {GetRuleIdsOfTextBlockPipe} from "./pipes";

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        MatIconModule,
        DragDropModule,

        ActionButtonModule,
        AutoFocusAfterInitModule,
        CollapsibleModule,
        CommonControlsModule,
        SelectModule,
        SharedPipesModule,
        SharedSettingsModule,
        SideMenuModule,
        TextBlockModule
    ],
    declarations: [
        TextBlockFormComponent,
        TextBlockListFormComponent,
        TextBlockSelectFormComponent,
        TextBlocksSettingsComponent,

        GetRuleIdsOfTextBlockPipe
    ],
    exports: [
        TextBlockFormComponent,
        TextBlockListFormComponent,
        TextBlockSelectFormComponent,
        TextBlocksSettingsComponent,

        GetRuleIdsOfTextBlockPipe
    ]
})
export class TextBlockSettingsModule {

}
