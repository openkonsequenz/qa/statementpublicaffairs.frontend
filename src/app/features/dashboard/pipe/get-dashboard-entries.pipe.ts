/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Pipe, PipeTransform} from "@angular/core";
import {IStatementTableEntry} from "../../../shared/layout/statement-table/model";
import {IStatementEntityWithTasks} from "../../../store/statements/model";
import {arrayJoin} from "../../../util/store";


/**
 * This function converts a list of statement entities to a list of statement table entries.
 * Which means the additional properties needed to display the statements in the dashboard lists are added to the list elements.
 */

@Pipe({name: "getDashboardEntries"})
export class GetDashboardEntriesPipe implements PipeTransform {

    public transform(value: IStatementEntityWithTasks[], editedByMe?: boolean, forMyDepartment?: boolean): IStatementTableEntry[] {
        return arrayJoin(value)
            .filter((statement) => statement?.info?.id != null)
            .filter((statement) => !editedByMe || statement.editedByMe)
            .map((statement) => {
                const {
                    mandatoryContributionsCount,
                    mandatoryDepartmentsCount,
                    optionalForMyDepartment,
                    completedForMyDepartment,
                    tasks,
                    info,
                    departmentStandIn
                } = statement;

                let contributionStatus = "-";
                if (Number.isFinite(mandatoryDepartmentsCount)) {
                    contributionStatus = `${Number.isFinite(mandatoryContributionsCount) ? mandatoryContributionsCount : 0}/${mandatoryDepartmentsCount}`;
                }

                let contributionStatusForMyDepartment = true;
                if (!completedForMyDepartment) {
                    contributionStatusForMyDepartment = optionalForMyDepartment ? null : false;
                }

                const {name, taskDefinitionKey} = {...arrayJoin(tasks)[0]};

                return {
                    ...info,
                    contributionStatus,
                    contributionStatusForMyDepartment,
                    currentTaskName: name == null ? taskDefinitionKey : name,
                    departmentStandIn: forMyDepartment ? departmentStandIn : undefined
                };
            });
    }

}
