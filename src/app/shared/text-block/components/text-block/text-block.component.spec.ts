/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {ComponentFixture, TestBed, waitForAsync} from "@angular/core/testing";
import {I18nModule} from "../../../../core/i18n";
import {TextBlockModule} from "../../text-block.module";
import {TextBlockComponent} from "./text-block.component";

describe("TextBlockComponent", () => {
    let component: TextBlockComponent;
    let fixture: ComponentFixture<TextBlockComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [
                TextBlockModule,
                I18nModule
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TextBlockComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should be created", () => {
        expect(component).toBeTruthy();
    });

    it("should track by index", () => {
        expect(component.trackByIndex(19)).toBe(19);
    });

    it("should emit appDeleteComment with the comment id", () => {
        spyOn(component.appValueChange, "emit").and.callThrough();
        component.valueChange("name", "Hugo", "input");
        expect(component.appValueChange.emit).toHaveBeenCalledWith({name: "<f:name>", newValue: "Hugo"});
    });

    it("should emit appTextInput and set editText to true", () => {
        spyOn(component.appTextInput, "emit").and.callThrough();
        component.editText = false;
        component.convertToTextInput();
        expect(component.appTextInput.emit).toHaveBeenCalled();
        expect(component.editText).toBeTrue();
    });

    it("should emit appTextChange with the given value", () => {
        spyOn(component.appTextChange, "emit").and.callThrough();
        const value = "test value";
        component.onInput(value);
        expect(component.appTextChange.emit).toHaveBeenCalledWith(value);
    });

    it("should emit appTextChange with empty string for freetext and otherwise undefined and set editText to false", () => {
        spyOn(component.appTextChange, "emit").and.callThrough();
        component.editText = true;
        component.appType = "text";
        component.revert();
        expect(component.appTextChange.emit).toHaveBeenCalledWith("");
        expect(component.editText).toBeFalse();

        component.editText = true;
        component.appType = "block";
        component.revert();
        expect(component.appTextChange.emit).toHaveBeenCalledWith(undefined);
        expect(component.editText).toBeFalse();
    });

    it("should set editText to false when input field loses focus", () => {
        component.editText = true;
        component.onFocusOut(new Event("focusout"));
        expect(component.editText).toBeFalse();
    });

    it("should not lose focus when revert button is clicked", () => {
        component.editText = true;
        const event = {relatedTarget: {name: "revertButton"}, preventDefault: () => null} as unknown as FocusEvent;
        spyOn(event, "preventDefault").and.callThrough();
        component.onFocusOut(event);
        expect(event.preventDefault).toHaveBeenCalled();
        expect(component.editText).toBeTrue();
    });

});
