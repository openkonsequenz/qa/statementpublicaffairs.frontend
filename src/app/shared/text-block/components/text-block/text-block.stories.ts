/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {TranslateModule} from "@ngx-translate/core";
import {boolean, text, withKnobs} from "@storybook/addon-knobs";
import {moduleMetadata, storiesOf} from "@storybook/angular";
import {I18nModule} from "../../../../core/i18n";
import {TextBlockModule} from "../../text-block.module";

storiesOf("Shared", module)
    .addDecorator(withKnobs)
    .addDecorator(moduleMetadata({
        imports: [
            TextBlockModule,
            TranslateModule,
            I18nModule
        ]
    }))
    .add("TextBlockComponent", () => ({
        template: `
            <div style="padding: 1em;">
                <app-text-block [appTextBlockData]="[{type: 'text', value: text}, {type: 'input', value: 'input'}]"
                                [appTitle]="title"
                                [appType]="'block'"
                                [appShowNewLine]="showNewLine"
                                [appErrors]="error ? [{message: 'Muss platziert werden nach:', ids: ['1', '2']}] : undefined"
                                [appShortMode]="shortMode">
                </app-text-block>
            </div>
        `,
        props: {
            title: text("Titel", "Textbaustein 1.0"),
            text: text("Text", "Hier steht ein Beispieltext"),
            error: boolean("Show error", false),
            showNewLine: boolean("Show new line button", true),
            shortMode: boolean("Shorten block to 2 lines", false),
        }
    }));
