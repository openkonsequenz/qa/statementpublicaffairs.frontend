/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Pipe, PipeTransform} from "@angular/core";

import {arrayJoin} from "../../../../util/store";
import {ITextBlockRenderItem} from "../../model/ITextBlockRenderItem";

/**
 * Takes in an array of TextBlockRender items and combines adjacent ones of type text into one.
 */
@Pipe({
    name: "combineBlockdataText"
})
export class CombineBlockdataTextPipe implements PipeTransform {

    public transform(blockData: ITextBlockRenderItem[]): ITextBlockRenderItem[] {
        const result: ITextBlockRenderItem[] = [];
        let text = "";
        for (const blockElement of arrayJoin(blockData)) {
            if (blockElement.type === "text") {
                text += (text !== "" ? " " : "") + blockElement.value;
            }
            const isLastElement: boolean = blockElement === blockData.slice(-1)[0];

            if (text !== "" && (isLastElement || blockElement.type !== "text")) {
                result.push({
                    type: "text",
                    value: text
                });
                text = "";
            }

            if (blockElement.type !== "text") {
                result.push(blockElement);
            }
        }
        return result;
    }
}
