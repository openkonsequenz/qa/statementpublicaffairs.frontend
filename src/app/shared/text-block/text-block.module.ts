/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {DragDropModule} from "@angular/cdk/drag-drop";
import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {TranslateModule} from "@ngx-translate/core";
import {CommonControlsModule} from "../controls/common";
import {DateControlModule} from "../controls/date-control";
import {SelectModule} from "../controls/select";
import {CollapsibleModule} from "../layout/collapsible";
import {GlobalClassToggleModule} from "../layout/global-class-toggle";
import {SharedPipesModule} from "../pipes";
import {TextBlockSelectComponent} from "./components/editor-textblock-select";
import {TextBlockComponent} from "./components/text-block";
import {TextBlocksListComponent} from "./components/text-block-list";
import {TextReplacementComponent} from "./components/text-replacement";
import {GetBlockDataFromArrangementPipe, GetBlockDataFromBlockModelPipe} from "./pipes/get-blockdata-array";

@NgModule({
    imports: [
        CommonModule,
        MatIconModule,
        CollapsibleModule,
        SelectModule,
        DateControlModule,
        DragDropModule,
        SharedPipesModule,
        TranslateModule,
        GlobalClassToggleModule,
        TranslateModule,
        MatButtonModule,
        CommonControlsModule
    ],
    declarations: [
        TextBlockComponent,
        TextBlockSelectComponent,
        TextBlocksListComponent,
        TextReplacementComponent,
        GetBlockDataFromArrangementPipe,
        GetBlockDataFromBlockModelPipe
    ],
    exports: [
        TextBlockComponent,
        TextBlockSelectComponent,
        TextBlocksListComponent,
        GetBlockDataFromArrangementPipe,
        GetBlockDataFromBlockModelPipe,
        TextBlocksListComponent,
        TextReplacementComponent
    ]
})
export class TextBlockModule {

}
