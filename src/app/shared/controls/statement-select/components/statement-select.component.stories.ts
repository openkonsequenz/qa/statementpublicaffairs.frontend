/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {RouterTestingModule} from "@angular/router/testing";
import {action} from "@storybook/addon-actions";
import {withKnobs} from "@storybook/addon-knobs";
import {moduleMetadata, storiesOf} from "@storybook/angular";
import {I18nModule} from "../../../../core/i18n";
import {createSelectOptionsMock, createStatementModelMock} from "../../../../test";
import {StatementSelectModule} from "../statement-select.module";

storiesOf("Shared / Controls", module)
    .addDecorator(withKnobs)
    .addDecorator(moduleMetadata({
        imports: [
            RouterTestingModule,
            I18nModule,
            StatementSelectModule
        ]
    }))
    .add("StatementSelect", () => ({
        template: `
        <div style="padding: 1em; height: 100%; width: 100%; box-sizing: border-box;">
            <app-statement-select
                [appSearchContent]="appOptions"
                [appStatementTypeOptions]="appStatementTypeOptions"
                [appValue]="appValue"
                (appValueChange)="appValueChange($event)">
            </app-statement-select>
        </div>
        `,
        props: {
            appValue: [3, 5],
            appStatementTypeOptions: createSelectOptionsMock(5, "StatementType"),
            appOptions: Array(20).fill(0).map((_, id) => createStatementModelMock(id, id % 5)),
            appValueChange: action("appValueChange")
        }
    }));
