/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {TranslateModule} from "@ngx-translate/core";
import {SearchbarModule} from "../../layout/searchbar";
import {StatementTableModule} from "../../layout/statement-table";
import {StatementSelectComponent} from "./components";
import {GetStatementEntriesForSelectPipe} from "./pipes";

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,

        StatementTableModule,
        SearchbarModule
    ],
    declarations: [
        StatementSelectComponent,
        GetStatementEntriesForSelectPipe
    ],
    exports: [
        StatementSelectComponent,
        GetStatementEntriesForSelectPipe
    ]
})
export class StatementSelectModule {

}
