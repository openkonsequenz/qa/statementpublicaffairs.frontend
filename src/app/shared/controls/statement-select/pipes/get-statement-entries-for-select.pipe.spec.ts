/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createStatementModelMock} from "../../../../test";
import {IStatementTableEntry} from "../../../layout/statement-table/model";
import {GetStatementEntriesForSelectPipe} from "./get-statement-entries-for-select.pipe";

describe("GetStatementEntriesForSelectPipe", () => {

    const pipe = new GetStatementEntriesForSelectPipe();

    it("should transform statement models to table entries", () => {
        const statements = Array(100).fill(0).map((_, id) => createStatementModelMock(id));
        const value = [19];
        const results: IStatementTableEntry[] = [...statements]
            .map((_, id) => ({..._, isSelected: id === 19}));
        expect(pipe.transform(statements, value)).toEqual(results);
    });

});
