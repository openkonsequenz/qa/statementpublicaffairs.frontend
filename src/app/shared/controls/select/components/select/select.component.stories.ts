/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {action} from "@storybook/addon-actions";
import {boolean, number, text, withKnobs} from "@storybook/addon-knobs";
import {moduleMetadata, storiesOf} from "@storybook/angular";
import {ISelectOption} from "../../model";
import {SelectModule} from "../../select.module";

storiesOf("Shared/Controls", module)
    .addDecorator(withKnobs)
    .addDecorator(moduleMetadata({imports: [SelectModule]}))
    .add("SelectComponent", () => ({
        template: `
            <app-select style="margin: 1em; width: 10em;" class="openk-info"
                [appDisabled]="appDisabled"
                [appOptions]="appOptions"
                [appPlaceholder]="appPlaceholder"
                [appValue]="appValue"
                (appValueChange)="appValueChange($event)">
            </app-select>
        `,
        props: {
            appValue: number("appValue", undefined),
            appOptions: Array(5).fill(0).map<ISelectOption>((_, value) => ({label: "Option " + value, value})),
            appPlaceholder: text("appPlaceholder", "Placeholder"),
            appDisabled: boolean("appDisabled", false),
            appValueChange: action("appValueChange")
        }
    }));
