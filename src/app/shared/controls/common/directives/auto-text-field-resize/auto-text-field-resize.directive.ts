/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import {Directive, ElementRef, HostListener, Input, OnChanges, SimpleChanges} from "@angular/core";

@Directive({
    selector: "[appAutoTextFieldResize]"
})
export class AutoTextFieldResizeDirective implements OnChanges {

    @Input()
    public appAutoResizeData: any;

    public constructor(public inputElement: ElementRef<HTMLInputElement>) {
    }

    @Input()
    public set value(value: string) {
        this.inputElement.nativeElement.value = value;
        this.resize();
    }

    @HostListener("input")
    public onInput() {
        this.resize();
    }

    public ngOnChanges(changes: SimpleChanges) {
        const keys: Array<keyof AutoTextFieldResizeDirective> = ["appAutoResizeData"];
        if (keys.some((key) => changes[key] != null)) {
            setTimeout(() => this.resize());
        }
    }

    public resize() {
        this.inputElement.nativeElement.style.height = "1px";
        this.inputElement.nativeElement.style.height = this.inputElement.nativeElement.scrollHeight + "px";
    }

}

