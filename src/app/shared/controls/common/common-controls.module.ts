/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import {NgModule} from "@angular/core";
import {AutoInsertTextFieldTokenDirective} from "./directives/auto-insert-text-field-token";
import {AutoTextFieldResizeDirective} from "./directives/auto-text-field-resize";
import {FormControlStatusDirective} from "./directives/form-control-status";

@NgModule({
    declarations: [
        FormControlStatusDirective,
        AutoTextFieldResizeDirective,
        AutoInsertTextFieldTokenDirective
    ],
    exports: [
        FormControlStatusDirective,
        AutoTextFieldResizeDirective,
        AutoInsertTextFieldTokenDirective
    ]
})
export class CommonControlsModule {

}
