/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {CommonModule} from "@angular/common";
import {MatIconModule} from "@angular/material/icon";
import {boolean, number, withKnobs} from "@storybook/addon-knobs";
import {moduleMetadata, storiesOf} from "@storybook/angular";

const templateInput = `
    <div style="display: flex; flex-flow: column; padding: 1em; justify-content: center; box-sizing: border-box;">
      <input type="text" value="Default" style="margin: 0.5em; box-sizing: border-box;"
             *ngFor="let class of [null, 'primary', 'info', 'success', 'warning', 'danger']"
             class="openk-input"
             [ngClass]="class == null ? null : 'openk-' + class"
             [disabled]="disabled"
             [value]="class == null ? 'default' : class"
             [style.max-width]="maxWidth + 'em'">
    </div>
`;

const templateButton = `
    <div style="display: flex; flex-flow: row wrap; padding: 1em; box-sizing: border-box;">
      <button class="openk-button"
        style="margin: 0.5em;"
        *ngFor="let class of [null, 'primary', 'info', 'success', 'warning', 'danger']"
        [disabled]="disabled"
        [class.openk-button-rounded]="rounded"
        [ngClass]=" class == null ? null : 'openk-' + class">

        <mat-icon *ngIf="rounded || withIcon">view_list</mat-icon>
        <ng-container *ngIf="!rounded"> {{class == null ? 'default' : class}} </ng-container>
      </button>
    </div>
`;

storiesOf("Shared/Controls", module)
    .addDecorator(withKnobs)
    .addDecorator(moduleMetadata({imports: [CommonModule, MatIconModule]}))
    .add("Input (Default HTML)", () => ({
        template: templateInput,
        props: {
            disabled: boolean("Disabled", false),
            maxWidth: number("Width", 20, {range: true, min: 0, max: 20, step: 1})
        }
    }))
    .add("Button (Default HTML)", () => ({
        template: templateButton,
        props: {
            disabled: boolean("Disabled", false),
            withIcon: boolean("With Icon", true),
            rounded: boolean("Rounded", false)
        }
    }));
