/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Pipe, PipeTransform} from "@angular/core";
import {objectToArray} from "../../../util/store";

@Pipe({name: "objKeysToArray"})
export class ObjKeysToArrayPipe implements PipeTransform {

    /**
     * Converts an object to an array containing its keys.
     */
    public transform<T extends object>(value: T, keepNullOrUndefined?: boolean): string[] {
        return objectToArray<T>({...value}, keepNullOrUndefined)
            .map((entry) => entry.key)
            .sort();
    }

}
