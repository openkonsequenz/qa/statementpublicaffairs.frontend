/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {FormArray, FormGroup} from "@angular/forms";
import {GetFormArrayPipe} from "./get-form-array.pipe";

describe("GetFormArrayPipe", () => {

    const pipe = new GetFormArrayPipe();

    it("should pick a form array from a form group", () => {
        const formArray = new FormArray([]);
        const formGroup = new FormGroup({array: formArray});

        expect(pipe.transform(formGroup, "array")).toBe(formArray);
        expect(pipe.transform(formGroup, "noArray")).not.toBeDefined();
        expect(pipe.transform(null, null)).not.toBeDefined();
    });
});
