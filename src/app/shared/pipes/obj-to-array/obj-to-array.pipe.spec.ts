/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {ObjToArrayPipe} from "./obj-to-array.pipe";

describe("ObjToArrayPipe", () => {

    const pipe = new ObjToArrayPipe();

    it("should convert an object to an array of its key value pairs", () => {
        const testObject = {
            prop1: "propValue1",
            prop2: undefined,
            prop3: null
        };
        expect(pipe.transform(testObject)).toEqual([{key: "prop1", value: "propValue1"}]);
        expect(pipe.transform(testObject, false)).toEqual([{key: "prop1", value: "propValue1"}]);
        expect(pipe.transform(testObject, true)).toEqual([
            {key: "prop1", value: "propValue1"},
            {key: "prop2", value: undefined},
            {key: "prop3", value: null}]
        );
        expect(pipe.transform(undefined)).toEqual([]);
        expect(pipe.transform(null)).toEqual([]);
    });
});
