/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAppConfiguration} from "../../../../core";
import {IStatementTableEntry} from "../model";
import {StatementTableAlertPipe} from "./statement-table-alert.pipe";

describe("StatementTableAlertPipe", () => {

    const pipe = new StatementTableAlertPipe({
        dashboard: {
            timeToShowWarningBeforeDueTimeInDays: 5,
            timeToShowWarningBeforeDepartmentsDueTimeInDays: 5
        }
    } as IAppConfiguration);

    it("should transform entries to boolean", () => {
        const entry: IStatementTableEntry = {} as IStatementTableEntry;
        expect(pipe.transform(null, null)).toBe(false);
        expect(pipe.transform(entry, false)).toBe(false);

        entry.dueDate = "2019-01-01";
        expect(pipe.transform(entry, false)).toBe(true);

        entry.dueDate = "2025-01-01";
        entry.departmentsDueDate = "2019-01-01";
        expect(pipe.transform(entry, true)).toBe(true);
        expect(pipe.transform(entry, false)).toBe(false);
    });

    it("should check if a given time span is due", () => {
        expect(pipe.isDue(null, 5)).toBe(false);
        expect(pipe.isDue(pipe.fallbackDueTimeInDays * (1000 * 60 * 60 * 24), 5)).toBe(false);
        expect(pipe.isDue((pipe.fallbackDueTimeInDays - 1) * (1000 * 60 * 60 * 24), 5)).toBe(true);
        expect(pipe.isDue(0, 5)).toBe(true);
    });

    it("should use fallback values if no config settings provided", () => {
        const pipeWithoutConfig = new StatementTableAlertPipe({
            dashboard: null
        } as IAppConfiguration);
        const entry: IStatementTableEntry = {} as IStatementTableEntry;
        expect(pipe.transform(null, null)).toBe(false);
        expect(pipe.transform(entry, false)).toBe(false);
    });

});
