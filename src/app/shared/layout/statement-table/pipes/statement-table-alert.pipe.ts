/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Inject, Pipe, PipeTransform} from "@angular/core";
import {APP_CONFIGURATION, IAppConfiguration} from "../../../../core";
import {momentDiff} from "../../../../util/moment";
import {IStatementTableEntry} from "../model";

@Pipe({name: "statementTableAlert"})
export class StatementTableAlertPipe implements PipeTransform {

    public readonly fallbackDueTimeInDays = 5;

    constructor(@Inject(APP_CONFIGURATION) public configuration: IAppConfiguration) {

    }

    public transform(value: IStatementTableEntry, useDepartmentsDueDate: boolean): boolean {
        const dueDate = useDepartmentsDueDate === true ? value?.departmentsDueDate : value?.dueDate;
        let timeToShowWarningInDays: number;
        if (this.configuration.dashboard == null) {
            timeToShowWarningInDays = this.fallbackDueTimeInDays;
        } else {
            timeToShowWarningInDays = useDepartmentsDueDate === true
                ? this.configuration.dashboard.timeToShowWarningBeforeDepartmentsDueTimeInDays
                : this.configuration.dashboard.timeToShowWarningBeforeDueTimeInDays;
        }

        if (dueDate == null) {
            return false;
        }
        return this.isDue(momentDiff(dueDate, new Date()), timeToShowWarningInDays ?? this.fallbackDueTimeInDays);
    }

    public isDue(diffInMs: number, timeToShowWarning: number): boolean {
        return Number.isFinite(diffInMs) ? diffInMs < (timeToShowWarning * (1000 * 60 * 60 * 24)) : false;
    }

}
