/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import {withKnobs} from "@storybook/addon-knobs";
import {moduleMetadata, storiesOf} from "@storybook/angular";
import {ListModule} from "../list.module";

const list = [
    {label: "Medianet (Planung/Bau)"},
    {label: "Medianet (Betrieb"},
    {label: "Wasser (Planung/Bau)"},
    {label: "Wasser (Betrieb)"},
    {label: "Allg. Baulandentwicklung (Planung, Bau)"},
    {label: "Allg. Baulandentwicklung (Betrieb)"},
    {label: "Gas Hochdruck (Planung, Bau)"},
    {label: "Gas Hochdruck (Betrieb)"},
    {label: "GDRM/KKS/ELEX (Planung/Bau)"},
    {label: "GDRM/KKS/ELEX (Betrieb)"}
];

storiesOf("Shared/Layout", module)
    .addDecorator(withKnobs)
    .addDecorator(moduleMetadata({imports: [ListModule]}))
    .add("ListComponent", () => ({
        template: `
            <div style="padding: 1em; height: 100%; width: 100%; box-sizing: border-box;">
                <app-list [appTitle]="'Allgemein'" [appListItems]="appListItems" [appIsDeletable]="true"></app-list>
                <app-list [appTitle]="'Regionalstelle Entenhausen'" [appListItems]="appListItems"></app-list>
            </div>
        `,
        props: {
            appListItems: list
        }
    }));


