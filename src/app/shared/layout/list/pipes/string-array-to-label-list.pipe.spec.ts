/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {StringArrayToLabelListPipe} from "./string-array-to-label-list.pipe";

describe("StringArrayToLabelListPipe", () => {

    const pipe = new StringArrayToLabelListPipe();

    describe("transform", () => {

        it("should add all array entries to objects as label property and return that new array", () => {

            const strings = [
                "first",
                "second",
                "third",
                "fourth"
            ];

            let result = pipe.transform(strings);

            expect(result).toEqual([
                {label: "first"},
                {label: "second"},
                {label: "third"},
                {label: "fourth"}
            ]);

            result = pipe.transform(null);
            expect(result).toEqual([]);

            result = pipe.transform(undefined);
            expect(result).toEqual([]);

            result = pipe.transform([]);
            expect(result).toEqual([]);
        });
    });
});

