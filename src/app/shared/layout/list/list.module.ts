/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {ListComponent} from "./components";
import {StringArrayToLabelListPipe} from "./pipes";


@NgModule({
    imports: [
        CommonModule,
        MatIconModule,
        MatButtonModule
    ],
    declarations: [
        ListComponent,
        StringArrayToLabelListPipe
    ],
    exports: [
        ListComponent,
        StringArrayToLabelListPipe
    ]
})
export class ListModule {

}
