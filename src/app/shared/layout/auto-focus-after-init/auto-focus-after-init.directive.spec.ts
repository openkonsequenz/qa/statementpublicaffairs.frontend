/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/


import {ElementRef} from "@angular/core";
import {AutoFocusAfterInitDirective} from "./auto-focus-after-init.directive";

describe("AutoFocusAfterInitDirective", () => {

    let directive: AutoFocusAfterInitDirective;

    it("should focus element ref after ngAfterViewInit", () => {
        const elementRef = {
            nativeElement: {
                focus: (options: FocusOptions) => null
            }
        } as ElementRef<HTMLElement>;
        const focusOptions: FocusOptions = {};
        const focusSpy = spyOn(elementRef.nativeElement, "focus");
        directive = new AutoFocusAfterInitDirective(elementRef);
        directive.appFocusOptions = focusOptions;
        directive.ngAfterViewInit();
        expect(focusSpy).toHaveBeenCalledWith(focusOptions);
    });

});
