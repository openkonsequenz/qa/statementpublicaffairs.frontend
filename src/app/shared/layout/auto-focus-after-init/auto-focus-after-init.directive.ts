/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {AfterViewInit, Directive, ElementRef, Input} from "@angular/core";

@Directive({
    selector: "[appAutoFocusAfterInit]"
})
export class AutoFocusAfterInitDirective implements AfterViewInit {

    @Input()
    public appFocusOptions: FocusOptions;

    public constructor(public elementRef: ElementRef<HTMLElement>) {

    }

    public ngAfterViewInit() {
        this.elementRef.nativeElement.focus(this.appFocusOptions);
    }

}
