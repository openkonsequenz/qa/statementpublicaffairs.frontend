/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {DOCUMENT} from "@angular/common";
import {Directive, Inject, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from "@angular/core";

@Directive({
    selector: "[appGlobalClass]"
})
export class GlobalClassToggleDirective implements OnInit, OnChanges, OnDestroy {

    @Input()
    public appGlobalClass: string;

    public constructor(@Inject(DOCUMENT) public document: Document) {

    }

    public ngOnInit() {
        this.addClass(this.appGlobalClass);
    }

    public ngOnChanges(changes: SimpleChanges) {
        if (changes.appGlobalClass != null) {
            const change = changes.appGlobalClass;
            this.removeClass(change.previousValue);
            this.addClass(change.currentValue);
        }
    }

    public ngOnDestroy() {
        this.removeClass(this.appGlobalClass);
    }

    public removeClass(classString: string) {
        if (typeof classString === "string") {
            this.document.body.classList.remove(classString);
        }
    }

    public addClass(classString: string) {
        if (typeof classString === "string") {
            this.document.body.classList.add(classString);
        }
    }


}
