/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Injectable} from "@angular/core";
import {BehaviorSubject, defer, Subject} from "rxjs";
import {distinctUntilChanged, map} from "rxjs/operators";
import {filterDistinctValues} from "../../../../util/store";
import {SideMenuDirective} from "../directives";
import {TSideMenuContentPosition} from "../TSideMenuContentPosition";

@Injectable({providedIn: "root"})
export class SideMenuRegistrationService {

    public content$ = defer(() => this.directive$).pipe(
        map(() => {
            const positions: TSideMenuContentPosition[] = ["top", "center", "bottom"];
            return positions.map((_) => this.getContentInPosition(_));
        })
    );

    public hasContent$ = defer(() => this.directive$).pipe(
        map((directives) => directives.length > 0),
        distinctUntilChanged()
    );

    public left$ = defer(() => this.directive$).pipe(
        map(() => this.getLeft()),
        distinctUntilChanged()
    );

    public title$ = defer(() => this.directive$).pipe(
        map(() => this.getTitle()),
        distinctUntilChanged()
    );

    public readonly resize$ = new Subject<any>();

    public style$ = defer(() => this.directive$).pipe(
        map(() => this.getStyle())
    );

    private readonly directivesSubject = new BehaviorSubject<SideMenuDirective[]>([]);

    private get directive$() {
        return this.directivesSubject.asObservable();
    }

    public register(directive: SideMenuDirective) {
        this.directivesSubject.next(filterDistinctValues([...this.directivesSubject.getValue(), directive]));
    }

    public deregister(directive: SideMenuDirective) {
        this.directivesSubject.next(this.directivesSubject.getValue().filter((_) => _ !== directive));
    }

    private getLeft(): boolean {
        return this.directivesSubject.getValue()
            .some((_) => _.appSideMenuLeft);
    }

    private getTitle(): string {
        return this.directivesSubject.getValue().reverse()
            .map((_) => _.appSideMenuTitle)
            .filter((_) => _)[0];
    }

    private getContentInPosition(position: TSideMenuContentPosition) {
        return this.directivesSubject.getValue().reverse()
            .find((_) => _.appSideMenu === position);
    }

    private getStyle() {
        return this.directivesSubject.getValue()
            .reverse()
            .map((_) => _.appSideMenuStyle)
            .find((_) => _ != null);
    }

}
