/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, Input} from "@angular/core";
import {boolean, text, withKnobs} from "@storybook/addon-knobs";
import {moduleMetadata, storiesOf} from "@storybook/angular";
import {SideMenuModule} from "../../side-menu.module";

@Component({
    selector: "app-side-menu-container-story",
    template: `
        <app-side-menu-container>

            <div>
                Content...
            </div>

        </app-side-menu-container>

        <div *appSideMenu="'top'; title: appTitle; left: appLeft;">
            Top
        </div>

        <div *appSideMenu="'center'">
            Center
        </div>

        <div *appSideMenu="'bottom'">
            Bottom
        </div>
    `
})
class SideMenuContainerStoryComponent {

    @Input()
    public appTitle: string;

    @Input()
    public appLeft: boolean;

}

storiesOf("Shared / Layout / Side Menu", module)
    .addDecorator(withKnobs)
    .addDecorator(moduleMetadata({
        imports: [
            SideMenuModule
        ],
        declarations: [
            SideMenuContainerStoryComponent
        ]
    }))
    .add("SideMenuContainerComponent", () => ({
        component: SideMenuContainerStoryComponent,
        props: {
            appLeft: boolean("Left", false),
            appTitle: text("Title", "Title")
        }
    }));
