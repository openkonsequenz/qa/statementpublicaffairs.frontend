/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, Input} from "@angular/core";
import {SideMenuRegistrationService} from "../../services";

@Component({
    selector: "app-side-menu-container",
    templateUrl: "./side-menu-container.component.html",
    styleUrls: ["./side-menu-container.component.scss"]
})
export class SideMenuContainerComponent {

    @Input()
    public appHideSideMenu: boolean;

    public content$ = this.registrationService.content$;

    public left$ = this.registrationService.left$;

    public hasContent$ = this.registrationService.hasContent$;

    public title$ = this.registrationService.title$;

    public style$ = this.registrationService.style$;

    public isOpen = true;

    constructor(public registrationService: SideMenuRegistrationService) {

    }

}
