/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {RouterTestingModule} from "@angular/router/testing";
import {action} from "@storybook/addon-actions";
import {boolean, number, text, withKnobs} from "@storybook/addon-knobs";
import {moduleMetadata, storiesOf} from "@storybook/angular";
import {ActionButtonModule} from "../action-button.module";

storiesOf("Shared / Layout", module)
    .addDecorator(withKnobs)
    .addDecorator(moduleMetadata({
        imports: [
            RouterTestingModule,
            ActionButtonModule
        ]
    }))
    .add("ActionButtonComponent", () => ({
        template: `
            <app-action-button
                style="margin: 1em; width: auto; max-width: 15em;"
                [appDisabled]="appDisabled"
                [appIcon]="appIcon"
                [appRouterLink]="asAnchor ? appRouterLink : null"
                [appStatementId]="appStatementId"
                [ngClass]="ngClass"
                (appClick)="appClick($event)">
                {{content}}
            </app-action-button>
        `,
        props: {
            content: text("content", "Button"),
            asAnchor: boolean("asAnchor", false),

            appDisabled: boolean("appDisabled", false),
            appIcon: text("appLoadingMessage", "redo"),
            appRouterLink: text("appRouterLink", "/details"),
            appStatementId: number("appStatementId", 19),
            appClick: action("appClick"),
            ngClass: text("ngClass", "openk-info")
        }
    }));
