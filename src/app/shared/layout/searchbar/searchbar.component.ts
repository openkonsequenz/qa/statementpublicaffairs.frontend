/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from "@angular/core";

@Component({
    selector: "app-searchbar",
    templateUrl: "./searchbar.component.html",
    styleUrls: ["./searchbar.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchbarComponent {

    @Input()
    public appPlaceholder: string;

    @Input()
    public appSearchText: string;

    @Input()
    public appIsLoading: boolean;

    @Output()
    public appSearch: EventEmitter<string> = new EventEmitter();

}
