/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {AbstractControl, AbstractControlOptions, AsyncValidatorFn, FormGroup, ValidatorFn} from "@angular/forms";

export function createFormGroup<TFormGroupType extends object>(
    controls: { [key in keyof TFormGroupType]: AbstractControl; },
    validatorOrOpts?: ValidatorFn | ValidatorFn[] | AbstractControlOptions | null,
    asyncValidator?: AsyncValidatorFn | AsyncValidatorFn[] | null
): FormGroup {
    return new FormGroup(controls, validatorOrOpts, asyncValidator);
}

/**
 * Trims a string value and returns a default value if it is empty.
 */
export function shrinkString(value: string, defaultValue?: string): string {
    if (typeof value !== "string") {
        return defaultValue;
    }
    const trimmed = value.trim();
    return trimmed.length > 0 ? trimmed : defaultValue;
}
