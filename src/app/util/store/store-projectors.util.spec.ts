/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/


import {selectArrayProjector, selectEntityWithIdProjector, selectPropertyProjector} from "./store-projectors.util";
import {TStoreEntities} from "./store.util";

describe("StoreProjectorUtil", () => {

    interface IStoreObject {
        property: number;
    }

    interface IStoreArray {
        array: number[];
    }

    it("selectEntityWithIdProjector", () => {
        const entity: IStoreObject = {property: 19};
        const state: TStoreEntities<IStoreObject> = {19: entity};
        const stateWithKey: { key?: typeof state } = {key: state};
        const defaultValue = {property: -19};

        const projector = selectEntityWithIdProjector<typeof state, number>(defaultValue);
        expect(projector(state, 19)).toBe(entity);
        expect(projector(state, 18)).toBe(defaultValue);
        expect(projector(null, 19)).toBe(defaultValue);

        const projectorWithKey = selectEntityWithIdProjector<typeof stateWithKey, "key", number>(defaultValue, "key");
        expect(projectorWithKey(stateWithKey, 19)).toBe(entity);
        expect(projectorWithKey(stateWithKey, 18)).toBe(defaultValue);
        expect(projectorWithKey(null, 19)).toBe(defaultValue);

    });

    it("selectPropertyProjector", () => {
        const state: IStoreObject = {property: 19};
        const defaultValue = -19;

        const projector = selectPropertyProjector<IStoreObject, "property">("property", defaultValue);
        expect(projector(state)).toBe(19);
        expect(projector({property: null})).toBe(defaultValue);
        expect(projector(null)).toBe(defaultValue);
    });

    it("selectArrayProjector", () => {
        const state: IStoreArray = {array: [19]};
        const defaultValue = [-19];

        const projector = selectArrayProjector<IStoreArray, "array">("array", defaultValue);
        expect(projector(state)).toBe(state.array);
        expect(projector({array: null})).toBe(defaultValue);
        expect(projector(null)).toBe(defaultValue);
    });

});
