/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

/**
 * Function that returns the store entitiy for the specified key and id. If no value for that key and id exist, returns the default value if
 * specified.
 */
export function selectEntityWithIdProjector<TState, TKey extends keyof TState, TId extends keyof TState[TKey]>(
    defaultValue: TState[TKey][TId], key: TKey
): (state: TState, id: TId) => TState[TKey][TId];
export function selectEntityWithIdProjector<TState, TKey extends keyof TState>(
    defaultValue?: TState[TKey]
): (state: TState, id: TKey) => TState[TKey];
export function selectEntityWithIdProjector<T>(defaultValue?: T, key?: string): any {
    return key == null ?
        (state, id) => state == null || state[id] == null ? defaultValue : state[id] :
        (state, id) => state == null || state[key] == null || state[key][id] == null ? defaultValue : state[key][id];
}

/**
 * Function that returns the object property for the specified key. If no value for that key exist, returns the default value if specified.
 */
export function selectPropertyProjector<TState, TKey extends keyof TState>(
    key: TKey,
    defaultValue?: TState[TKey]
): (state: TState) => TState[TKey] {
    return (state) => state == null || state[key] == null ? defaultValue : state[key];
}

/**
 * Function that returns the object property for the specified key only if it is an array.
 * If no value for that key exist, returns the default value if specified.
 */
export function selectArrayProjector<TState, TKey extends keyof TState>(
    key: TKey,
    defaultValue?: TState[TKey] & Array<any>
): (state: TState) => TState[TKey] {
    return (state) => state == null || !Array.isArray(state[key]) ? defaultValue : state[key];
}
