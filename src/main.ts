/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {enableProdMode} from "@angular/core";
import {platformBrowserDynamic} from "@angular/platform-browser-dynamic";

import {AppModule} from "./app/app.module";
import {APP_CONFIGURATION} from "./app/core/configuration";
import {environment} from "./environments/environment";

if (environment.production) {
    enableProdMode();
}

function configListener() {
    try {
        const configuration = JSON.parse(this.responseText);

        // pass config to bootstrap process using an injection token
        platformBrowserDynamic([
            { provide: APP_CONFIGURATION, useValue: configuration }
        ])
            .bootstrapModule(AppModule)
    .catch(err => console.error(err));

    } catch (error) {
        console.error(error);
    }
}

function configFailed(evt) {
    console.error("Error: retrieving config.json");
}

const request = new XMLHttpRequest();
request.addEventListener("load", configListener);
request.addEventListener("error", configFailed);
request.open("GET", "./assets/config/config.json");
request.send();
